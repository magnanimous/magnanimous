# Feature List
This document serves as a list of Magnanimous features.  It is not a
complete or exhaustive list, but is a good overview.

## Debugging

1. **Stub Marking.** *Complete*  Devs can place a single call inside a
   function like this to generate a console warning in the front-end.
   ```
   function _handle_something () {
     $M->debug->mark_stub();
   }
   ```
   In the browser console, the dev will see a warning like this:
   ```
   (!) Method stub not implemented: Plugin\Dashboard->_handle_something
   ```
   This can be useful for devs to make sure they're not leaving cruft in
   files, or as a reminder to implement features, etc.
1. **Browser Console Interop.** *Complete*  Users can print
   JSON-serialzed PHP data directly to the browser console for
   manipulation with simple commands.  Supports `debug`, `log`, `info`,
   `warn`, and `error`.

## JavaScript

In an effort to help WordPress plugin developers create more interactive
experiences in WordPress, Magnanimous provides a very rich set of tools for
working with JavaScript in plugins.

1.  **JavaScript Magnanimous.** *Complete* Magnanimous provides PHP classes
    for streamlined plugin development, of course.  But in addition it
    provides a mirror of those classes for browser-side plugin
    development.  That means that scripts injected through Magnanimous are
    provided with a `var Magnanimous` JavaScript (pseudo global) variable
    that comes pre-equipped to do a variety of tasks like AJAX, template
    publishing, and so on.  This happens completely transparently to the
    dev.

1.  **AJAX.** *Complete* Magnanimous creates a transparent AJAX experience,
    requiring nearly zero configuration.  All that needs to be provided is a
    server-side PHP handler for an AJAX call coming in.  In a subsequent
    version, this should be *completely* inferred, allowing any call to come
    in and an *ad hoc* handler to be used (perhaps returning `{}` or a 4xx
    error?) in the event that the inferred handler does not exist.

1.  **Data Sharing.** *Complete* Magnanimous provides facilities for passing
    data back and forth between the server and client environments.

1.  **Script Registration.** *Complete* Devs can "register" a
    script.

1.  **Script Requires.** *Complete* Devs can mark a script as a
    dependency.

1.  **Automated Versioning** *Complete* If the script is not
    versioned, a dynamic version is applied for cache-busting.  A new
    version is only generated when the file is modified.

1.  **Minification Detection** *Complete* Automatically uses the minified
    css file if one is available (by the same name with `.min.js`).

1.  **Script Injection.** *Complete* A plugin can inject a script
    inline, rather through the normal "inclusion" mechanism.  This fully
    supports variable interpolation.

1.  **Client-side Libraries.** *Partial*  Magnanimous provides a growing set
    of client-side libaries automatically included.  These are written in
    ES6 and transpiled (and minified) using BabelJS and other tools.  These
    libraries can be expanded by developers for rich client-side experience
    from plugins.  Combined with Magnanimous AJAX and Form handling, this
    yields a lot of power.

1.  **ES6/Node Integration.** *Partial/Manual*  I have implemented partial
    support for developing client-side micro-apps in Node/ES6.  This is
    transpiled and deployed as minified apps.  It needs to be more tightly
    coupled as a full step in plugin deployment.

## CSS

1. **Stylesheet Registration.** *Complete* Devs can "register" a
   stylesheet.  This is a wrapper around WordPress's
   `wp_register_style()` hook.

1. **Stylesheet Requires.** *Complete* Devs can mark a stylesheet as a
   dependency.  This wraps (and replaces) the `wp_enqueue_style()` hook
   in WordPress.

1. **Automated Versioning** *Complete* If the stylesheet is not
   versioned, a dynamic version is applied for cache-busting.  A new
   version is only generated when the file is modified.

1. **Minification Detection** *Complete* Automatically uses the minified
   css file if one is available (by the same name with `.min.css`).

1. **Stylesheet Injection.** *Complete* A plugin can inject a stylesheet
   inline, rather through the normal "inclusion" mechanism.

1. **Sass/LESS/etc.** *Partial/Manual* I have implemented partial
    support for deploying CSS through Sass (with Compass).  This is
    transpiled and deployed as minified css.  It needs to be more tightly
    coupled as a full step in plugin deployment.

## HTML

1. **Templates.** *Complete* HTML templates can be created (on
      disk or in a variable) and displayed to the browser with variable
      expansion.  This is not to be confused with the similar feature of
      JavaScript HTML templates for rendering documents client-side
      through JavaScript with jQuery.

1. **Forms.** *Complete* Magnanimous provides various ways 
   1. **Automated Form Handler Inference.** *Complete*  When a form is
      injected into a WordPress page, the form's handler is connected,
      with some pre-processing done on the form data.
   1. **Unique Identification Tokens.** *Create*  Each form is given a
      unique form identity token to ensure that automated handling
      inference works as expected.
   1. **Default Handlers.** *Complete* When a valid handler isn't
      provided, a default handler is provided that shows a simple, clear
      message that the form's handler has not yet been implemented.
   1. **Template Files.** *Complete* Forms can be created through form
      templates files on disk with variable expansion.
   1. **Data population.** *Complete* Properly formatted forms can be
      auto-filled with initial data (including the data just submitted)
      when rendered.
   1. **Auto form wrapping.** *Complete* Form templates do not need to
      have (and **should not have**) `<form></form>` tags.  Magnanimous
      provides the automatically, with special magical markup to make
      fancy things happen.

## SQL
1. **Script isolation.** *Complete*.  Place a SQL script into a directory.
   That script can be a pure SQL script (with variable expansion
   capabilities).
1. **Variable expansion.** *Complete*.  Allows for variables to be
   used when calling SQL scripts through the Magnanimous DB handler.
   1. **User-Defined Variables** *Complete* Users may define variables in
      scripts and have it explained.
      1. **Ordered Expansion** *Complete*.  Magnanimous replaces `%s` with
         the next ordinal value until values are exhausted.
      1. **Named Expansion** *Complete*.  Magnanimous allows users to pass
         in a variable expansion array with arbitrarily named values
         (prefixed with '%' by convention).
   1. **Magic Variables** *Ad hoc* The "magic variable" `%{wpdb_prefix}`
      expands to the WordPress installation's database prefix.
1. **Query Preparation.** *Planned*.  This feature should allow for
   queries to be written as normal, but then use the DBI interface to
   "prepare" the query and execute it with variables.  This will require a
   bit of script inspection, as the standard SQL approach for this is to
   use `?` symbols for variables and order-based variables only.  

   This is very do-able, but requires a bit of tinkering.  The main issue
   is to first survey the query to find the expandable variables, then
   convert the associative array into an list with repeated values (if
   needed).  Optionally, magnanimous could compile the scripts in an
   at-once step before plugin publication.  This is probably the preferred
   option.

## Utilities

1. **String Manipulation.** *Complete*  Magnanimous provides some lightly
   opinionated string manipulation tools that help the toolkit do certain
   things quickly and well.
   1. **Positional Variable Expansion.** *Compelte*  Replace the `%s`
      special code with each provided value, in turn.
   1. **Named Variable Expansion.** *Complete* Replace named variables
      (prefixed by `%` by convention) with values provided through an
      array.
1. **Paths.** *Complete* Magnanimous determines various server, disk, and
   URI paths automatically, and uses them as needed.

1. **Unique IDs.** *Complete* Given a string, create an ID unique to the PHP
   process.  Note that this is *not* a UUID or similar.  Rather, this is
   simply a garaunteed unique string.

## Files

1. **Memoized File Reading.** *Complete* Reads text files off the disk
   with memoization.  Of course, this sits on top of any caching PHP
   does.

## Plugin Configuration

1. **Plugin Shadowing.** *Complete*  Creates a reference to an instance
   of a plugin configuration tailored to the developed plugin.  The
   reference can be called by name.

## Themes

Magnanimous provides some tools for making themes better and easier.
Support for this is fairly immature at the moment.

1. **Theme Shadowing.** *Partial* Creates a reference to an instance of a
   theme configuration tailored to the developed theme.  The reference may
   be called by name.

## UI Dashboard Components

Magnanimous provides a set of tools for streamlining dashboard UI
development.

1.  **Menu construction.** *Complete*
    1. **Handler inference.** *Complete* Automatically infers the handler
       functions (user-callable routines) for menu items.
    1. **Page Link generation.** *Complete*  Given a url path (like
       'admin/tools' in the form of function arguments (e.g.
       `get_page_link('admin','tools')`) to yield a url link for that admin
       page.

1.  **Notices.** *Complete* WordPress provides a way for plugins to alert
    users of various information through *notifications*.  Magnanimous
    streamlines this experience.


