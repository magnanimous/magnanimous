# Known Issues

## Documentation

* There's no technical documentation yet, just tutorials.

* The tutorials are incomplete (~40% coverage).

## Testing

### Unit Tests

* There are none (and there need to be).  The big problem here is I just
  don't know enough PHP community to know how unit or e2e/integration
  testing is done.

### e2e Tests

* There are none (and there need to be).  The big problem here is I just
  don't know enough PHP community to know how unit or e2e/integration
  testing is done.


## Admin Notices

* Admin notices render before forms are handled, so if you have an admin
  notice like, "Your tables haven't been created" and a form to create them,
  the admin notice will still show after the form submission that creates
  the tables.  The table handles it fine, but the notice is no longer
  accurate.

* Admin notices trigger on WordPress's "heartbeat" directed towards
  admin-ajax.php.

## JavaScript
*No known issues.*


## CSS
*No known issues.*
