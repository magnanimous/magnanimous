
## Current TODOs

* Integrate ES6/Babel, Sass/LESS, templating, etc.
* Migrate to a full "publication" step, but with full usability during
  development.
* Everywhere there's not a handler or something like that, Magnanimous
  should make that clear in the browser.  For example, M::JS::Ajax.php line
  41.
* In javascript app injections, the $template psuedo-global should be
  subsumed into the $state.template space.  $state should be made available
  with whatever tools are required.
* Compile standard sql queries to prepared queries.
* Create WordPress fallbacks for non-secure sql queries.


## Older TODOs (pre November 2017).

* REDUCE BOILERPLATE.  Add various kinds of factories.  See if
  monkey-patching php classes is possible.  

  Some patterns to reduce:

  * Creating AJAX handlers

        function register_ajax () {
          $this->M->js->ajax->_register_ajax_handler(
            'add-subscription-item',
            [$this, '_ajax_add_subscription_item']
          );

          $this->M->js->ajax->_register_ajax_handler(
            'update-subscription-quantity',
            [$this, '_ajax_update_subscription_quantity']
          );

          $this->M->js->ajax->_register_ajax_handler(
            'update-subscription-product',
            [$this, '_ajax_update_subscription_product']
          );
        }

  This could be reduced to something more like:

      function register_ajax () {
        $M->js->ajax->handle($this, 'add_subscription_item');
        $M->js->ajax->handle($this, 'update_subscription_quantity');
        $M->js->ajax->handle($this, 'update_subscription_product');
      }

  or, better,

      function register_ajax () {
        $M->js->ajax->handle([
          'add_subscription_item',
          'update_subscription_quantity',
          'update_subscription_product',
        ]);
      }

  And do the rest automatically (except, obviously, the dev has to define
  the function).

* Use the [JsonSerializable::jsonSerialize
  interface](http://nl3.php.net/manual/en/jsonserializable.jsonserialize.php)
  to serialize objects for `M->debug->console->log/etc`.  
  * Monkey-patching may be an option so users don't have to serialize every
    obejct themselves.  E.g. Creating a base class that extends the class to
    serialize, then doing some kind of introspection (?) to automate
    serialization.
* Smart file prep.  If an href has, say, `href="%DASHBOARD_URI(My Plugin,
  Settings, Colors)"` automatically replace it with the right link.
* Document the js dependencies system (register/require).
* Document script injection
* Allow script injection to specify requirements.
* Make it so you can use the following in injected (or required) script
  files:

    var sprintf_lite = require("sprintf_lite")

* Document the excellent prepare_str pattern that lets us use an object
  against a well-structured string.  This doesn't quite work yet because of
  the percents.

      $data = [
        'id': 123,
        'name':  My Thing
      ];
      $str = '<option value="%id">%name</option>';
      $out = prepare_str($str, $data);

* Note that script some file prep for html seems to strip out comments.
* Consider switching from %s to $s.  Reasons:
  * JavaScript also allows $ as a native prefix (so no box-property notation
    needed).
  * It already looks like a variable to PHP people.
  * Maybe not because:  is MySQL cool with it?
* Magnanimous.js should reflect the same Plugin/Theme dichotomy that .php
  does.

* Explain (to devs?  to self?) why Magnanimous\JS\AJAX::$ajax_handlers is a
  private static var instead of an instance property.  The reason is this
  lets themes and plugins share a registry.  Without this, theme handlers
  were getting overwritten.

* Add to the "prepare_str" call in M->DB so special things like wp_posts are
  automatically replaced with the real table name.  This would look like
  this:

  * Require users to use the magic form %TABLE(wp_posts)
  * In the prepare_str call, add all tables to the hash, like so:

        [
          '%TABLE(wp_posts)' => get_table_name('wp_posts'),
          ...
        ]

    And so on, with whatever that function is that gets a table's real name
    (or prefix or whatever).

* Make 'Embeddable Magnanimous' so each plugin can ensure its own version,
  if the developer wants.
* Allow registered JavaScripts to have css requirements.

* Magnanimous\DB - Add the possibility to do query modifiers like WHERE ?

* Evaluate security and utility considerations in M\DB\SELECT, UPSERT, etc.
  use of ->get_results and ->query

* Do a levinshtein distance on missing files to make recommendations about
  which they may have meant.

* Document file uploads (!!!).
* Let the Magnanimous plugin define one or more upload directories for the
  plugin.  These should be created, then given the right permissions and
  ownership.
* Clean up the file upload and file post code with better:
  * comments
  * documentation
  * error handling
* Let file uploading auto-generate the right inputs (either through php or
  javascript).
  * Express this stuff in dreamcode.
* Change permissions of uploaded files.
* Integrate with the media database.

* Create a concept of html templates distinct from html files.  This needs
  native php support.

* In M->js->inject() consider enhancing templates.  For example:
  * automatically read the $params.templates hash and turn it into
    automatically usable junks.  Here's a suggestion:

        // Dreamcode:
        $template.upload();      // returns the $($params.templates.upload)
        $template.upload('str'); // returns the same as a string

* Add a 'templates' option to the $options array of $M->js->inject (the
  third argument).  This would take something like [name => 'str'] which it
  would translate with $M->html->template('str'), then prepare it in the
  javascript $template.name().

* DB tools like INSERT and DELETE should return something useful.

* Consider passing some stuff into the ajax handler like:
  * The access level (priv/nopriv)
  * The POST/GET params
  * Other similar stuff

* Use Magnanimous as a plugin publication mechanism for developers.  Go to
  the Magnanimous dashboard place and click "Create Plugin" from one of the
  installed plugins.  Same for themes.

* Put explicit mention of places where Magnanimous could be seen to
  facilitate or even encourage breaking of the coding best practices (such
  as
  [https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/#database-queries](here)).




