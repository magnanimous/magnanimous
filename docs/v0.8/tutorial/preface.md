# The Magnanimous Tutorial

## Example Plugin

For the purposes of this tutorial, we will be creating an example plugin
called "Task Master", a to-do list for your blog.

Next:  [Chapter 1: Starting a Plugin](./chapter-01.md)

