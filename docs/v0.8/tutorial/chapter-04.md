# Chapter 4:  JavaScript

For many WordPress plugins, JavaScript is essential.  Magnanimous seeks to
make working with JavaScript completely painless.  It does this by removing
the burden of script management from the developer.  By providing
strategically designed tools, developers can add JavaScript, manage AJAX
queries, and more without much thought.

## Injecting Scriptlets

Small JavaScript scriptlets can be injected into a page at any point,
completely painlessly.  Consider the example of having a page with a
specific, unique key feature.  Three seconds after the page loads, you want
to change the color of the thick border exactly once, to call attention to
it.  

If the HTML and CSS look like this:

    <style type="stylesheet">
    #main-visual-feature { border: 10px double grey; }
    </style>

    <div id="main-visual-feature">
      <p>Pay attention to this!</p>
    </div>

The JavaScript (using jQuery) may look something like this:

    setTimeout(function () {
      $('#main-visual-feature')
        .animate(
          {backgroundColor: 'goldenrod'},
          1000,
          "linear",
          function () {
            $(this).after("<a href='more.html'>Find out more!</a>");
          }
        )
    }, 3000);

This snippet is very short.  A little too long to comfortably embed in the
PHP file, but a little too short make into a library (and only used once in
the site, for a unique purpose).

The Magnanimous approach is "Script Injection."  Injecting JavaScript
directly into a page is clean and simple.

## Libraries

### Registering libraries

### Using Libraries

## AJAX

--------

Prev:  [Chapter 2:  Forms](./chapter-03.md)

Next:  [Chapter 4:  TBD](./chapter-05.md)

