# Chapter 3:  Handling Forms

In the previous section, we generated and displayed some HTML.  That's a
method for conveying information from you to the user.

Sometimes you need to get information directly from the user.  There are
lots of ways to do it (forms, AJAX, websockets, etc.) but we're going to
focus on the venerable HTML form in this chapter.  WordPress (and, indeed,
PHP itself) was built with the concepts of forms in mind and they are still
a staple communication mechanism.

## Example:  A Complete Magnanimous Form

Let's take a look at a complete form, first.  Then we'll go into an
explanation.

This form is going to allow users to perform an administrative action:
creating the necessary database tables for our plugin.

First, our PHP.  In our plugin's class file, we set up an admin dashboard
menu (we'll look more at this later):

    class MyFunPlugin {
      private $M;

      function __construct ($params=[]) {
        $this->M = \Magnanimous::Plugin('MyFunPlugin');
        $this->M->ui->dashboard->add_menu('MyFunPlugin');
      }

      function _handle_myfunplugin () {

        $html = <<<EOHTML
        <h1>My Fun Plugin:  Admin Settings<h1>
        <p>Use this page to create your table.</p>
        <%form:create-table>
    EOHTML;

        echo $this->M->html->prepare($html, [
          '<%form:create-table>' => $this->M->form->create(
            'MyFunPlugin Form: Create Tables',
            '<button type="submit">Create Table</button>',
            [$this, '_handle_create_table']
          )
        ]);
      }

      function _handle_create_table () {
        $this->M->db->CREATE('quizzes');
      }
    }

Believe it or not, this is a complete plugin.  It creates the admin
dashboard menu item and page, creates the HTML in the page, adds the form,
processes the form (creating the desired db table when the form is
submitted), and re-renders the page.

## Before Forms, plain HTML.

Let's get started by adding content to the next section of our menu, the
"Settings" section.  We'll do this exactly the same way as the Overview
section was handled in [the previous chapter](./chapter-02.md), so we'll
skip the explanations here.
  
    // ... somewhere in our plugin
    function _handle_settings () {
      $this->M->html->prepare_file('dashboard/settings');
    }

And our (very simple) HTML.

    <!-- ./resources/html/dashboard/settings.html -->
    Call us at 1-800-OLD-TECH if you want us to change your settings!

Here, we give them an alternate technology to use to contact us, since we
can't receive input from forms yet.

### Adding a Form File

Let's make things easier on everyone by letting the user talk through the
website.  First, let's add a form to our HTML file.

*Note: We don't need to use files for this, we could be generating strings
in php, say, and using those as our form bodies.  But Magnanimous's file
tools are really, really convenient, so we'll keep using those for the sake
of this tutorial.*

    <!-- ./resources/html/dashboard/settings.html -->
    <p>I see the site's text in %s, but would like to see it in</p>
    <select name='text_color'>
      <option value="red"   sel_red  > red   </option>
      <option value="green" sel_green> green </option>
      <option value="blue"  sel_blue > blue  </option>
    </select>.
    <button type='submit'>Save</button>

For the moment, ignore the extra attributes (`sel_red` and such) in the
`<option>` tags.

Now when you re-load the page, you see the text with a single form select
element.  The problem here is that there's no actual form (that is, there
are no `<form></form>` tags).  As often as possible, Magnanimous likes to
handle the boilerplate of things for you.  In this case, it means you type
the controls and inputs you care about and forget about what Magnanimous
handles the dirty work.

Let's see that now-- we'll use Magnanimous's *Form* tool (`->form`).  See
[the Form Toolkit documentation](../documentation/Magnanimous/Forms.md) for
technical information.

### Piping it through the Magnanimous Form Tool

We update our plugin so that instead of echoing our file, we use Magnanimous
to wrap it with `<form></form>` tags and other magical candy:

    // ... somewhere in our plugin
    function _handle_settings () {
      // Read our HTML from the file, as before.
      $form_body = $this->M->html->prepare_file('dashboard/settings');

      // Now let Magnanimous's form tool provide the magic.
      // The three arguments here are:  $name, $html, $handler
      $M->form->create(
        'Update Settings',
        $form_body,
        [$this, '_handle_form_update_settings']
      );
    }

### Creating a Form Submission Handler

We specified that we want another function, called
`_handle_form_update_settings` to manage the form parameters.  Let's create
that class method now.  There's some really cool magic in here that we'll
explain afterwards, but for now, take a look.

    function _handle_form_update_settings () {
      // Do something with this new value.
      $this->save_setting('text_color', $_POST['text_color']);

      // Return some metadata.
      $color = $_POST['text_color'];
      return [ "sel_$color" => 'selected' ];
    }

#### Saving the Data

The first line is pretty self-explanatory.  When the user submits this data,
just process it.  We won't go into depth about that here, but you can save
this to a database, print something to the screen, send an e-mail, or
whatever else you like.

#### Updating the Form

Remember before we said to ignore the extra tags, `sel_red`, `sel_green`,
`sel_blue`?  Here's where they come in.

Look at the return value from the handler.  It's a *map* of values.  These
values are string replacements for the form itself.  The next time the form
is processed, these replacements will be made (universally) in the text.

So, let's say the selected color was `green`.  Then our return value will
be

    [ 'sel_green' => 'selected' ]

That means our `<select>` will look like this before processing:

<div class='demo demo-html'>
  I see the site's text in PLACEHOLDER, but would like to see it in
  <select name='text_color'>
    <option value="red"   sel_red  > red   </option>
    <option value="green" sel_green> green </option>
    <option value="blue"  sel_blue > blue  </option>
  </select>.
  <button type='submit'>Save</button>
</div>

    I see the site's text in PLACEHOLDER, but would like to see it in
    <select name='text_color'>
      <option value="red"   sel_red  > red   </option>
      <option value="green" sel_green> green </option>
      <option value="blue"  sel_blue > blue  </option>
    </select>.
    <button type='submit'>Save</button>

and like this after:

<div class='demo demo-html'>
  I see the site's text in PLACEHOLDER, but would like to see it in
  <select name='text_color'>
    <option value="red"   sel_red  > red   </option>
    <option value="green" selected > green </option>
    <option value="blue"  sel_blue > blue  </option>
  </select>.
  <button type='submit'>Save</button>
</div>

    I see the site's text in PLACEHOLDER, but would like to see it in
    <select name='text_color'>
      <option value="red"   sel_red  > red   </option>
      <option value="green" selected > green </option>
      <option value="blue"  sel_blue > blue  </option>
    </select>.
    <button type='submit'>Save</button>

The latter version is what will be printed to the screen in our controller.
Because XHTML allows for superfluous tags (like `sel_red`) that are included
but not recognized, it doesn't hurt to have them.  By changing `sel_green`
to `selected`, the form will magically update to reflect what we just did.

Beautiful.

-----

A similar technique can be used for other inputs.  For example, consider a
text input:

    // ./resources/html/my_form.html
    <input type='text' name='fruit' placeholder='Name a fruit' />

Notice there's no explicit value.  That means when the form is first
displayed, it will just have a blank value (showing the placeholder).

Now we put into our plugin the following form creation and handler.

    // ... in our plugin
    function show_form () {
      echo $M->form->create(
        'My text form',
        'my_form',
        [$this, '_handle_my_form']
      );
    }

    function _handle_my_form () {
      $fruit = $_POST['fruit'];
      return ["name='fruit'" => "name='fruit' value='$fruit'"];
    }

The first time the form is displayed, the text field will be empty.  The
second every subsequent time (if it has been submitted), it will be updated
with the value submitted!

--------

Prev:  [Chapter 2:  Adding Content](./chapter-02.md)

Next:  [Chapter 4:  JavaScript](./chapter-04.md)

