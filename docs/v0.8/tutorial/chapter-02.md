# Chapter 2:  Adding Content

In the previous section we created a quick plugin with a top-level dashboard
menu item.  However, it didn't have any content.  Let's look at how to do
that now.

### Refactoring

First, let's re-factor our plugin a little to put the business logic into
the plugin class.

Our first task is to make it easy to work with Magnanimous.  A good design
pattern for this is to give your plugin an `M` property.  This should not
point to an instance of the Magnanimous class; rather it should be an
instance of your Magnanimous Plugin object. 

[[TODO: Provide docs on how the plugin object works.]]

You may be tempted to make this global, but resist the urge.  If there are
other plugins with the same idea, you'll be clobbering each other.

    // ./lib/TaskMaster.php
    class TaskMaster {
      private $M; // This is our Magnanimous toolkit.

      __construct ($params=[]) {
        $this->M = Magnanimous::Plugin('Task Master');
      }

      function add_menu () {
        $this->M->ui->dashboard
          ->add_menu('Task Master' , [$this , '_handle_main'    ])
            ->add_sub('Overview'   , [$this , '_handle_overview'])
            ->add_sub('Settings'   , [$this , '_handle_settings'])
            ->add_sub('Credits'    , [$this , '_handle_credits' ]);
      }
    }

Notice two key points:

1.  The `Plugin` class method is also a getter.  By referring to the plugin
    by name, we are able to get the corresponding Magnanimous toolkit
    object.  That's why in our main.php we load this plugin after the class
    object.  An alternative would be to create the Magnanimous toolkit
    object during construction.
1.  We can conveniently ignore the internal rigging required to hook into
    WordPress's systems.  It turns out Magnanimous has some features for
    letting you specify this in great detail, but if you don't, *it just,
    plain works*.

Then, back in our main plugin file, we have a cleaner, smaller footprint.


    // main.php
    <?php

    // Configure Magnanimous.
    $M = Magnanimous::Plugin(
      'Task Master',
      [ 'debug'  => TRUE,
        'plugin' => [
          'base' => __FILE__
        ]
      ]
    );

    // Create an instance of our WordPress plugin.
    require_once('./lib/TaskMaster.php');

    $TaskMaster = new TaskMaster();
    $TaskMaster->add_menu();

    ?>

Now everything works just as before, but we have cleaner separation of
concerns, and some nice embedded conveniences in our TaskManager class.

### Debugging Tools

If you're paying close attention, you may have noticed that the Magnanimous
setup changed: we added a `'debug' => TRUE` entry.  Magnanimous's debug
tools provide some beautiful magic for debugging.  Let's take a quick look
at some of that now.

In our `TaskMaster` class, we created a handler, `_handle_main`, that
controls what is printed when we visit that dashboard page.  Go to that
function now and add this code.

    function _handle_main () {
      $this->M->debug->console->log(['foo' => 'bar']);
    }

Now, open your browser's console and go to the menu page.  Your console
displays a *traversable* object, because Magnanimous's debugging allows you
to access the browser's console natively.  

Take your time; I know this can be an emotional moment.

#### Helpful Pattern

Here's a helpful pattern for debugging:  create a temporary global (or super
global, or whatever) variable that stores Magnanimous's console.  Like this:
  
    // ... in main.php, after you configure Magnanimous.
    global $console = $M->debug->console;

Then while you're developing, everything's easy:

    // ... in your class
    function my_function ($myvar) {
      global $console;
      $console->log($myvar);
    }

### More on Debugging

Magnanimous's `->debug` also provides the other browser console methods,
`error`, `warn`, etc.  In addition, it has facilities for doing file-based
debugging (e.g. log files), and more.  We'll take a closer look at that
later. 

### Adding Content to our Menu:  The HTML Tool

Right now, our dashboard menu is bare.  Let's add some content.  First,
we'll do a purely information page on the "Overview" submenu.

Magnanimous also provides an HTML tool to help when you just need to display
some straight HTML.  It's simple and easy.

*Remember:  Magnanimous is a toolkit.  If you simply want to close your
<?php> tag and create HTML in your function, that's your perogative.  You
aren't* forced *to be magnanimous, we just provide tools to help you if you
want to be!*

Let's take a look at our `_handle_overview()` method.

    // ... in TaskMaster.php
    function _handle_overview () {
      echo $this->M->html->prepare_file('plugin-overview');
    }

That's all, you're done!  Well, not *quite*, but you're done with the php
side!  

If you re-load the page now, you'll notice you get an error warning of a
missing file.

This is one of the places where Magnanimous gets slightly opinionated.  When
you wrote `html->prepare_file('plugin-overview')`, Magnanimous knew to
interpret "plugin-overview" as a file name.  It goes to look in exactly one
place for that file (relative to your plugin's root):

    ./resources/html/plugin-overview.html

Let's go ahead and create that now, with the following content.

    <!-- ./resources/html/plugin-overview.html -->
    <h1>Task Master</h1>

    <h2>Overview</h2>

    <p>
    Task Master is a tool for letting readers of a WordPress blog know what
    tasks are coming up next.
    </p>

    <p>
    There are lots of new features coming soon.
    </p>

    <p>
    We'll let you know when each feature is released!
    </p>

Now save the file and re-load the browser page.  See?  All your content is
magically inserted!  One of the great things about this approach is that it
takes advantage of PHP's built-in file caching to make this super fast.

Importantly, you can create arbitrary sub-paths below the opinionated
directory.  For example, if you want a bit more structure, you can
"namespace" your files like this:

    $M->html->prepare_file('dashboard/task-master/overview');

and then your html file would be found in

    ./resources/html/dashboard/task-master/overview.html

This is especially useful when working with lots of files that have various
relationships between them.

### Getting Dynamic

But what if you want to show something dynamic in the page?  How can you do
that?

For example, let's say we wanted to keep that content, but instead of saying
"lots of new features", give a dynamic number of features currently in the
queue?

One magnanimous approach would simply split the file in two, print the first
file, then print the custom string, then print the second file:

    // ... in TaskMaster.php
    function _handle_overview () {
      echo $this->M->html->prepare_file('plugin-overview-part1');
      echo "<p>There are " 
        . $this->features_in_queue()
        . " features in progress.</p>";
      echo $this->M->html->prepare_file('plugin-overview-part2');
    }
    
This approach uses the Magnanimous tool `$this->M->html->prepare_file()`
still, but there are some more powerful ways to use this tool in the
toolbox.

#### Sequential Interpolation

Magnanimous provides a cleaner approach.  First, let's make a small edit to
the HTML file.

    <!-- ./resources/html/plugin-overview.html -->
    <h1>Task Master</h1>

    <h2>Overview</h2>

    <p>
    Task Master is a tool for letting readers of a WordPress blog know what
    tasks are coming up next.
    </p>

    <p>
    There are %s features in progress.
    </p>

    <p>
    We'll let you know when each feature is released!
    </p>

See the change?  That middle line now has a variable placeholder (`%s`) and
a slightly re-worded sentence.  And what's in our PHP?  Remember, we
previously had this:

    // ... in TaskMaster.php
    function _handle_overview () {
      echo $this->M->html->prepare_file('plugin-overview');
    }

Now we have this:

    // ... in TaskMaster.php
    function _handle_overview () {
      echo $this->M->html
        ->prepare_file('plugin-overview', $this->features_in_queue());
    }

Fantastic!  Our HTML file is actually an *interpolable template* that can
undergo variable substitution.  We can interpolate as many variables as we
like; each `%s` is replaced with the next value in the argument list.  See
[the Magnanimous `prepare_file`
documentation](./docs/Magnanimous/HTML#prepare_file) for more details.

#### Named Interpolation

Magnanimous takes this a step further as well.  In addition to simple
*sequential interpolation*, you may used *named interpolation*.  Here's the
same example, using named interpolation.


    <!-- ./resources/html/plugin-overview.html -->
    ... this is all the same as in the previous example ...
    <p>
    There are %feature_count features in progress.
    </p>
    ... and the bottom is the same as well ...

And in our PHP code:

    // ... in TaskMaster.php
    function _handle_overview () {
      echo $this->M->html
        ->prepare_file('plugin-overview', ['%count' => $this->features_in_queue()]);
    }

<div style="background-color: orange">
<div>TODO</div>

* At some point, we want to support Markdown as well.
* Named template interpolation.

</div>

Prev:  [Chapter 1:  Starting a Magnanimous Plugin](./chapter-01.md)

Next:  [Chapter 3:  Handling Forms](./chapter-03.md)

