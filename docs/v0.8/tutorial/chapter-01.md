# Chapter 1:  Starting a Magnanimous Plugin

## Creating a Magnanimous Plugin

Magnanimous runs alongside your plugin and does not require special
integration.  You do not have to use it as a base class, you do not have to
use it to create things for you.

In some cases, it will be much more helpful if you configure it.  Here's a
good, typical development configuration for a Magnanimous plugin.  For the

    // main.php
    $M = Magnanimous::Plugin(
      'Task Master',
      [ 'plugin' => [ 'base' => __FILE__ ] ]
    );

This is very straight-forward:  Simply use the class method "Plugin" to
register a new plugin named 'Task Master'.  The 'base' option lets
Magnanimous know how to orient itself to the plugin's location on the disk.

<div style="background-color: orange">
<div> TODO </div>

* List all configuration options (e.g. debug).
* Talk about the Magnanimous Built-Ins:
  * -&gt;debug
  * -&gt;css
  * -&gt;ui
  * ... etc.
</div>

You're pretty much done.  Now you can do magic in your plugin!

Let's take a look at some of that magic.

## Creating a Dashboard Menu for your Plugin

Creating a top-level dashboard menu is easy:

    // Assume $M is our Magnanimous plugin.
    $M->ui->dashboard->add_menu('Task Master', 'my_handler');

That's it.  No, really, that's it.

But what about sub-menu items?  Easy as pie:

    // A menu with submenu items.
    $M->ui->dashboard
      ->add_menu('Task Master' , '_handle_main')
        ->add_sub('Overview'   , '_handle_overview')
        ->add_sub('Settings'   , '_handle_settings')
        ->add_sub('Credits'    , '_handle_credits' );
 
That's all there is to it.  Rendering is handled automatically, hooking is
managed for you, authorization is done magically.

### Hiding menu items

By default, if you create a menu as in the example above, the top menu item
-- Task Master-- will also be a sub-menu item.  This leads to a menu that
looks like this:

    Task Master
      Task Master
      Overview
      Settings
      Credits

If you would like to hide the duplicated sub-menu item, just pass a third
parameter to the `add_menu()` function, like this: 

    // A menu with submenu items.
    $M->ui->dashboard
      // This third argument means "Show as a submenu item: FALSE"
      ->add_menu('Task Master' , '_handle_main', FALSE)
        ->add_sub('Overview'   , '_handle_overview')
        ->add_sub('Settings'   , '_handle_settings')
        ->add_sub('Credits'    , '_handle_credits' );

This defaults to TRUE to keep consistency with WordPress default behavior.

## Chapter Summary

So far, we have:

* Set up our WordPress plugin.
* Set up the Magnanimous plugin that will service our WordPress plugin.
* Created a top-level menu with submenus.

Our files look like this.  First, in our main plugin file:

    // main.php
    <?php

    // Configure Magnanimous.
    $M = Magnanimous::Plugin(
      'Task Master',
      ['plugin' => ['base' => __FILE__]]
    );

    // Create an instance of our WordPress plugin.
    require_once('./lib/TaskMaster.php');
    $TaskMaster = new TaskMaster();

    // Create our top-level menu.
    $M->ui->dashboard
      ->add_menu('Task Master' , '_handle_main'     )
        ->add_sub('Overview'   , '_handle_overview' )
        ->add_sub('Settings'   , '_handle_settings' )
        ->add_sub('Credits'    , '_handle_credits'  ) ;


    ?>

And in our WordPress Plugin Class:

    // ./lib/TaskMaster.php
    class TaskMaster {
      // This is a stub for now.
    }

In the next section, we'll refactor a bit and look at some new features.


Prev:  [Preface](./preface.md)

Next:  [Chapter 2:  Adding content](./chapter-02.md)

