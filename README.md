# Magnanimous WordPress Development

*WordPress for the rest of us.*

# What is *Magnanimous*?

Magnanimous is a lightly opinionated *toolkit* for creating WordPress plugins.

## Toolkit vs. Framework

The difference between a toolkit and a framework is that a framework
requires you to use its facilities.  Its a frame around how you work.  A
toolkit provides tools that you may use when you like, but are not bound to use.

Magnanimous is a toolkit.  You can develop your plugins as normal, and
whenever you like, for any feature--large or small--simply use a Magnanimous
feature.

# Origin &amp; Current Status

Magnanimus is currently at a *late-alpha* to *early-beta* stage, as is
reflected in the current release version of **v0.8.11**.  

The software started as a personal project when I offered to help a few
friends with customizing their wordpress installations for businesses.  I
had dome a bit of WordPress work in the past, but never heavy plugin
development.  When I delved deeply into plugin development, I found that
there were quite a few WordPress idosynchrasies that--while stable and
preserving much-needed backwards compatibility--made many elements of it
counter-intuitive for someone with modern web development sensibilities.

This makes sense, since WordPress was originally released in 2003.  A lot
has changed since then, and many, many WordPress installations are on shared
hosts, older environments, and so on.

As I was writing plugins and themes, I started writing utilities to make
hard things easier and ugly things prettier.  Before too long, it became a
good toolkit for rapid WordPress plugin and theme development.  After a
quick re-factor to make it class-based, it became more portable and more
useful.

# Road Map

## Version 0.9

There are three main tasks for v0.9: 

* Implement unit tests covering the majority of the codebase.
* Enable multiple versioning of the toolkit in a single WordPress
  installation.
* Create a dashboard menu for Magnanimous.

## Version 1.0

Version 1.0.0 is expected to be the first mature, stable release of Magnanimous.
The project will be considered mature and stable when the following
conditions are met:

* There is complete unit test coverage for:
  * all PHP code features
  * all JavaScript code features
  * all SASS code features

* There is complete documentation for:
  * all PHP code features
  * all JavaScript code features
  * all SASS code features

* Multiple versions of Magnanimous can be utilized in different plugins in a
  single WordPress installation without conflict.

* Tooling for build processes are in place.

* Automated (file watching) compilation is available for:
  * SASS/SCSS
  * ES6 -> Browser
  * SQL queries to secure, prepared statements.

* Build processes are available for:
  * Plugins
  * Themes
  * Magnanimous

## Subsequent Versions

Right now, some javascript is embedded directly into the php code.  This
should be pulled out into javascript (ES6) files, then during the build
process, compiled and integrated into the PHP as appropriate.

The eventual intent is to offer related services for-profit so developers
can reduce plugin and theme development time by an order of magnitude.

# Documentation and Help Resources

These resources are currently incomplete.  They are a high priority and are
being actively developed as of July 2018.

* **Tutorial.** An introductory tutorial that covers most of the concepts in
  Magnanimous is available in [the Magnanimous Tutorial](./docs/latest/tutorial/chapter-01.md).
* **Documentation.** Technical docs for the various features is a work in
  progress and can be found in [the Magnanimous Docs](./docs/latest/documentation/index.html).

In addition, an incomplete [TODO list](./docs/latest/TODO.md) is available.
