<?php 

/*
  Plugin Name:  Magnanimous
  Plugin URI: http://getmagnanimous.com
  Description: A plugin to make WordPress plugin development easier.
  Author: Sir Robert Burbridge
  Version: 0.8.15
  Author URI: http://sirrobert.net
*/

// Do this before anything else.
require_once('Magnanimous.php');

?>
