Magnanimous = {};

;(function () {
  var $ = jQuery;

  var PluginRegistry = {};

  Magnanimous = function (params) {
    this.ajax_url    = params.ajax_url;
    this.ajax_action = params.ajax_action;
    this.plugin_name = params.name;
    this.ajax = new AJAX(this);
    this.pr = PluginRegistry;
  };

  Magnanimous.Plugin = function (name, params) {
    if (!params) {
      params = params || {};
      var noparams = true;
    }

    if (name && !PluginRegistry[name]) {
      if (noparams) {
        console.group('Creating a new Magnanimous plugin without parameters');
        console.warn(
          'Caution:  Creating a new plugin (' + name + ') without parameters.',
          'Perhaps you have a typo in the name of the plugin?'
        );
        var results = [];
        for (var plugin in PluginRegistry) {
          var distance = Magnanimous.levenshtein(name, plugin);
          if (distance <= 3) {
            results.push({distance: distance, name: plugin});
          }
        }
        results = results.sort(function (a,b) {
          return (a.distance < b.distance) ? -1
          : (a.distance > b.distance)      ?  1
          :                                   0;
        });

        var final = [];
        for (var i = 0; i < results.length; i++) {
          final.push(results[i].name);
        }
        console.warn('Similar registered plugins:', final.join(', '));
        console.groupEnd();
      }
      params.name = name;
      PluginRegistry[name] = new Magnanimous(params);
    }

    if (name) {
      return PluginRegistry[name];
    }

    console.error(
      "[Magnanimous]: You called Magnanimous.Plugin() without", 
      "an argument.  To find a Magnanimous plugin, please use",
      "Magnanimous.Plugin(pluginName) -- or to register a new", 
      "plugin, please use Magnanimous.Plugin(pluginName, params)."
    );
  }

  // For parallelism between the php side (in which this is a static method
  // on the Magnanimous class) and the JS side in which that's not really
  // important.
  Magnanimous.prototype.plugin = Magnanimous.Plugin;

  Magnanimous.levenshtein = function levenshtein(a, b) {
    var t = [], u, i, j, m = a.length, n = b.length;
    if (!m) { return n; }
    if (!n) { return m; }
    for (j = 0; j <= n; j++) { t[j] = j; }
    for (i = 1; i <= m; i++) {
      for (u = [i], j = 1; j <= n; j++) {
        u[j] = a[i - 1] === b[j - 1] ?
          t[j - 1]                     :
          Math.min(t[j - 1], t[j], u[j - 1]) + 1;
      } t = u;
    } return u[n];
  }

  //*******************************************************
  //
  //  A Magnanimous class for handling AJAX.  This basically lets you ignore
  //  routing completely except for knowing the actual action you want, but
  //  it completely resolves collisions by privately namespacing your AJAX
  //  calls.
  function AJAX (M) {
    this.M = M;
  }

  AJAX.prototype.post = function (action, data, callback) {
    var M = this.M;

    var p = {
      action        : M.ajax_action + "_ajax",
      plugin_action : action,
      data          : data,
      dataType      : 'json'
    };

    $(document).ready(function () {
      $.post(
        M.ajax_url,
        p,
        function () {
          var json = JSON.parse(arguments[0] || 'null');
          callback(json);
        }
      );
    });
  };

  AJAX.prototype.upload = function (action, data, callback) {
    var M = this.M;

    if (!action) {
      console.log('An action to handle file uploads must be provided.');
      return;
    }

    if (!data && !callback) {
      callback = function () {}
    }

    if ((typeof data) === 'function') {
      callback = data;
      data = null;
    }

    if ((typeof callback) !== 'function') {
      callback = function () {};
    }

    // Create some form data.
    var formdata = new FormData();
    formdata.append('action', M.ajax_action + '_ajax');
    formdata.append('plugin_action', action);

    if (!data) {
      data = {};
    }
    
    for (var d in data) {
      formdata.append(d, data[d]);
    }

    $.ajax({
      url: M.ajax_url,
      type: "POST",
      data: formdata,
      dataType: 'json',
      processData: false,
      contentType: false,
      success: callback
    });

  };


  AJAX.prototype.upload2 = function (action, input, data, callback) {
    var M = this.M;

    // Create some form data.
    var formdata = new FormData();
    formdata.append('action', M.ajax_action + '_ajax');
    formdata.append('plugin_action', action);
    formdata.append('upload', $(input)[0].files[0]);

    if (!data) {
      data = {};
    }
    
    for (var d in data) {
      formdata.append(d, data[d]);
    }

    $.ajax({
      url: M.ajax_url,
      type: "POST",
      data: formdata,
      dataType: 'json',
      processData: false,
      contentType: false,
      success: callback
    });

  };







  AJAX.prototype.download = function (action, data, callback) {
    var M = this.M;

    if (!action) {
      console.log('An action to handle file uploads must be provided.');
      return;
    }

    if (!data && !callback) {
      callback = function () {}
    }

    if ((typeof data) === 'function') {
      callback = data;
      data = null;
    }

    if ((typeof callback) !== 'function') {
      callback = function () {};
    }

    var formdata = new FormData();
    formdata.append('action', M.ajax_action + '_ajax');
    formdata.append('plugin_action', action);

    if (!data) {
      data = {};
    }
    
    for (var d in data) {
      formdata.append(d, data[d]);
    }

    $.ajax({
      url: M.ajax_url,
      type: "POST",
      data: formdata,
      dataType: 'text',
      processData: false,
      contentType: false,
      success: callback
    });

  };

})();
