<?php

require_once 'Magnanimous/CSS.php';
require_once 'Magnanimous/DB.php';
require_once 'Magnanimous/Debug.php';
require_once 'Magnanimous/Forms.php';
require_once 'Magnanimous/HTML.php';
require_once 'Magnanimous/JS.php';
require_once 'Magnanimous/Module.php';
require_once 'Magnanimous/Path.php';
require_once 'Magnanimous/Plugin.php';
require_once 'Magnanimous/Theme.php';
require_once 'Magnanimous/UI.php';
require_once 'Magnanimous/Utilities.php';

class Magnanimous {
  public $js;
  public $html;
  public $form;

  public $db;
  public $util;
  public $plugin;
  public $theme;
  public $path;

  public $debug;

  public $ui;

  // This is how we get directories (like JavaScript and CSS).
  public $MagnanimousDir = __DIR__ . '/';

  function __construct ($params=[]) {
    // If we turned on the debugger explicitly, load it up.
    // Putting the debugger first lets us use it to help debug the other
    // classes.
    if (array_key_exists('debug', $params) && $params['debug']) {
      // As a shorthand, we can turn on the debugger with 'debug' => TRUE,
      // rather than setting parameters.  In that case, just send in empty
      // params.
      if (gettype($params['debug']) == 'boolean') {
        $params['debug'] = [];
      }
      $this->debug = new Magnanimous\Debug($this, $params['debug']);
    }

    if (array_key_exists('plugin', $params)) {
      $this->plugin = new Magnanimous\Plugin($this,$params['plugin']);
      $this->path   = new Magnanimous\Path($this->plugin);
    }

    if (array_key_exists('theme', $params)) {
      $this->theme = new Magnanimous\Theme($this,$params['theme']);
      $this->path  = new Magnanimous\Path($this->theme);
    }

    $this->js   = new Magnanimous\JS($this);
    $this->css  = new Magnanimous\CSS($this);
    $this->html = new Magnanimous\HTML($this);

    $this->db   = new Magnanimous\DB($this);
    $this->util = new Magnanimous\Utilities($this);
    $this->form = new Magnanimous\Forms($this);

    $ui_params = [];
    if (array_key_exists('ui', $params)) {
      $ui_params = $params['ui'];
    }
    $this->ui   = new Magnanimous\UI($this, $ui_params);
  }

  private static $ModuleRegistry = [];
  function create_module ($params) {
    $name = $params['name'];
    $module = new Magnanimous\Module($this, $params);
    self::$ModuleRegistry[$name] = $module;

    return $module;
  }


  private static $PluginRegistry = [];
  static function Plugin ($name, $params=[]) {
    $Registry = self::$PluginRegistry;

    // Make sure we have a plugins array.
    if (!array_key_exists('plugin', $params)) {
      $params['plugin'] = [];
    }
    // Make sure the plugin has a name.
    if (!array_key_exists('name', $params['plugin'])) {
      $params['plugin']['name'] = $name;
      }

    // If we haven't registered this plugin, do it now.
    if (!array_key_exists($name, $Registry)) {
      // If another name wasn't provided for the plugin (for various
      // internal uses), use the registry lookup identifier.
      self::$PluginRegistry[$name] = new Magnanimous($params);
    }

    // Return it out of the registry.
    return self::$PluginRegistry[$name];
  }

  private static $ThemeRegistry = [];
  static function Theme ($name, $params=[]) {
    $Registry = self::$ThemeRegistry;

    // Make sure we have a themes array.
    if (!array_key_exists('theme', $params)) {
      $params['theme'] = [];
    }
    // Make sure the theme has a name.
    if (!array_key_exists('name', $params['theme'])) {
      $params['theme']['name'] = $name;
    }

    // If we haven't registered this theme, do it now.
    if (!array_key_exists($name, $Registry)) {
      // If another name wasn't provided for the theme (for various
      // internal uses), use the registry lookup identifier.
      self::$ThemeRegistry[$name] = new Magnanimous($params);
    }

    // Return it out of the registry.
    return self::$ThemeRegistry[$name];
  }

  function path_to_resource ($filename) {
    return $this->MagnanimousDir . $filename;
  }

}

?>
