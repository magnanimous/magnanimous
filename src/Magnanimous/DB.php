<?php
namespace Magnanimous;

class DB {

  private $dbh;
  private $M;

  function __construct ($M, $params=[]) {
    global $wpdb;
    global $current_user;

    $this->M = $M;
    $this->dbh = $wpdb;
  }

  function fields_to_obj ($result, &$obj, $fields) {
    if (!$result) {
      return;
    }

    foreach ($fields as $field) {
      $obj->{$field} = $result->{$field};
    }
  }

  function file ($name) {
    return $this->M->util->file->read("resources/sql/$name.sql");
  }

  // Returns a boolean indicating whether the table exists.
  function table_exists ($name) {
    global $wpdb;
    $name   = $wpdb->prefix . $name;
    $sql    = "SHOW TABLES LIKE '$name'";
    $result = $wpdb->query($sql);

    return ($result == 0) ? FALSE : TRUE;
  }

  // This is a debugging function.  It allows you to render the query for
  // inspection of the final product.
  function render_query ($type, $name, ...$args) {
    $query = $this->prepare_file($type, $name, ...$args);
    $query = $this->standardize_query($query);
    return $query;
  }

  function prepare ($str, ...$args) {
    // Just for kicks, let null values be treated like NULL values.  Why
    // not, we're being magnanimous, right?
    foreach ($args as &$arg) {
      if ($arg === null) {
        $arg = 'NULL';
      }
    }

    $output = $this->M->util->str->prepare($str, ...$args);
    $output = str_replace("'NULL'", 'NULL', $output);
    return $output;
  }

  function prepare_file ($type, $name, ...$args) {
    $parts = explode('/', $name);
    $last  = array_pop($parts);
    $added = array_push($parts, "${type}_${last}");
    $name  = join('/', $parts);
    return $this->prepare($this->file($name), ...$args);
  }

  // Standardize the query by adding things like the table prefixes.
  function standardize_query ($str) {
    return $this->prepare($str, [
      '%{wpdb_prefix}' => $this->dbh->prefix
    ]);
  }

  // You can just omit $args to get the original file.
  function SELECT ($name, ...$args) {
    $query = $this->render_query('SELECT', $name, ...$args);
    return $this->dbh->get_results($query);
  }

  // The same as SELECT but assumes you only want one row.
  function SELECT_ONE ($name, ...$args) {
    $query  = $this->render_query('SELECT', $name, ...$args);
    $result = $this->dbh->get_results($query);
    return array_shift($result);
  }

  function UPSERT ($name, ...$args) {
    $query = $this->render_query('UPSERT', $name, ...$args);
    return $this->dbh->get_results($query);
  }

  function INSERT ($name, ...$args) {
    $query = $this->render_query('INSERT', $name, ...$args);
    $this->dbh->query($query);
    return $this->dbh->insert_id;
  }

  function CREATE ($name, ...$args) {
    return $this->CREATE_TABLE($name, ...$args);
  }

  function CREATE_TABLE ($name, ...$args) {
    $query = $this->render_query('CREATE', $name, ...$args);
    return $this->dbh->get_results($query);
  }

  function MIGRATE ($versions, ...$args) {
    if (gettype($versions) == "string") {
      $name = $versions;
    }
    // This currently naieve, only letting you end up with one pair.  I
    // would prefer it to be able to do a chain of migrations, like:
    //
    //   $M->db->MIGRATE([
    //     "v0.0.1" => "v0.0.2",
    //     "v0.0.2" => "v0.3.0",
    //     "v0.3.0" => "v1.0.0"
    //   ]);
    //
    // that would perform a chain of migrations.
    else if (gettype($versions) == "array") {
      foreach ($versions as $from => $to) {
        $name = "$from-$to";
      }
    }

    $query = $this->render_query('MIGRATE', $name, ...$args);
    return;
  }

  function UPDATE ($name, ...$args) {
    $query = $this->render_query('UPDATE', $name, ...$args);
    return $this->dbh->get_results($query);
  }

  function DELETE($name, ...$args) {
    $query = $this->render_query('DELETE', $name, ...$args);
    return $this->dbh->query($query);
  }

}
