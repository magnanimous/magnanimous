<?php
namespace Magnanimous\UI;

require_once 'Dashboard/MenuItem.php';
require_once 'Dashboard/Notices.php';

class Dashboard {
  private $M;

  public $notices;
  public $menu = [];

  function __construct ($M, $params=[]) {
    $this->M = $M;
    $this->notices = new Dashboard\Notices($M);
  }

  function add_menu ($arg1, $arg2=null, $show_item=TRUE) {
    $params = [];

    if ($arg2 == FALSE) {
      $arg2 = null;
      $show_item = FALSE;
    }

    $params['permissions'] = $show_item ? NULL : 'hide_menu_item';

    // If we're calling this method, then we also need to render the menu at
    // the right time in WordPress's render cycle.  Let's add an event
    // listener here.
    add_action('admin_menu', [$this, '_render_menus']);

    if (gettype($arg1) == 'string') {
      if ($arg2) {
        $params['menu_text']   = $arg1;
        $params['handler']     = $arg2;
      } else {
        $params['menu_text'] = $arg1;

        // In this case, we have not gotten a handler, just a menu text.
        // Let's infer the handler from the menu text.

        // A menu item like, 'My Submenu' should be turned into the handler
        // [$this, '_handle_my_submenu'].  Note that the $this here is of
        // the caller object, not this Magnanimous object.  
        //
        // The fact that we are assuming the name of the function is
        // _handle_<transformed-menu-text> is one of the cases in which
        // Magnanimous is 'lightly opinionated.'  Of course, you don't have
        // to use this convention-- simply pass in your own handler.

        $obj = debug_backtrace()[1]['object'];
        $str = preg_replace('/\W+/', '_', $arg1);
        $str = preg_replace('/^_+/', '', $str);
        $str = preg_replace('/_+$/', '', $str);
        $str = '_handle_' . strtolower($str);
        $params['handler'] = [$obj, $str];
      }
    } elseif (gettype($arg1) == 'array') {
      $params = $arg1;
    }
    
    $this->menu[$params['menu_text']] =
      new Dashboard\MenuItem($this->M, $params);

    // Return the newly created menu item from the lookup.
    return $this->menu[$params['menu_text']];
  }

  function _render_menus () {
    // Loop through the created menus and render them.
    foreach ($this->menu as $name => $menu) {
      $menu->render();
    }
  }

  // Given a path, get a link to the described menu page.
  function get_page_link (...$args) {
    $post = join('_', $args);
    $post = strtolower($post);
    $post = preg_replace('/\W+/', '_', $post);
    return admin_url('admin.php') . "?page=_$post";
  }

}

?>
