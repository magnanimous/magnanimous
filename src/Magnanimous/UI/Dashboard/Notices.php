<?php
namespace Magnanimous\UI\Dashboard;

class Notice {
  public $content;
  public $type;

  function __construct ($params) {
    $this->_set_property_from_array('content', $params);
    $this->_set_property_from_array('type'   , $params);
  }

  private function _set_property_from_array ($property, $array) {
    // If it's in array, it gets into $this.  If it's not, then $this->...
    // remains unset.  This is desirable rather than having some default
    // value here, especially because not everything gets a default value
    // (like ->parent_id).
    if (array_key_exists($property, $array)) {
      $this->$property = $array[$property];
    }
  }
}

class Notices {

  private $M;
  public $list;

  function __construct ($M) {
    $this->M = $M;
    $this->list = array();

    add_action('admin_notices', [$this, '_render']);
  }

  function create ($type="info", $content="This is a blank notice.") {
    $notice = new Notice([
      'type' => $type,
      'content' => $content
    ]);

    array_push($this->list, $notice);
    return $this;
  }

  function _render () {
    foreach ($this->list as $notice) {
			echo '<div class="notice notice-' . $notice->type .'">';
		  echo  $notice->content;
      echo '</div>';
    }
  }

}

?>
