<?php
namespace Magnanimous\UI\Dashboard;

class MenuItem {

  private $M;

  public $page_title;
  public $menu_text;
  public $permissions;
  public $unique_id;
  public $handler;
  public $parent_id;

  public $parent;
  public $submenu = [];

  function __construct ($M, $params=[], $parent=null) {
    $this->M = $M;

    $this->_set_property_from_array('page_title' , $params);
    $this->_set_property_from_array('menu_text'  , $params);
    $this->_set_property_from_array('permissions', $params);
    $this->_set_property_from_array('unique_id'  , $params);
    $this->_set_property_from_array('handler'    , $params);
    $this->_set_property_from_array('parent_id'  , $params);

    $this->parent = $parent;

    $this->_infer_missing_properties();
  }

  function add_sub ($arg1, $arg2=null) {
    $params = [];

    // If $arg1 is a string, then $arg1 is the menu text and $arg2 is the
    // handler function, or else there's no menu text and just a handler
    // function.
    if (gettype($arg1) == 'string') {
      if ($arg2) {
        $params['menu_text'] = $arg1;
        $params['handler']   = $arg2;
      } else {
        $params['menu_text'] = $arg1;

        // In this case, we have not gotten a handler, just a menu text.
        // Let's infer the handler from the menu text.

        // A menu item like, 'My Submenu' should be turned into the handler
        // [$this, '_handle_my_submenu'].  Note that the $this here is of
        // the caller object, not this Magnanimous object.  
        //
        // The fact that we are assuming the name of the function is
        // _handle_<transformed-menu-text> is one of the cases in which
        // Magnanimous is 'lightly opinionated.'  Of course, you don't have
        // to use this convention-- simply pass in your own handler.

        $obj = debug_backtrace()[1]['object'];
				$str = preg_replace('/\W+/', '_', $arg1);
				$str = preg_replace('/^_+/', '', $str);
				$str = preg_replace('/_+$/', '', $str);
				$str = '_handle_' . strtolower($str);
				$params['handler'] = [$obj, $str];
      }
    }
    // If $arg1 is an array, though, then just treat it as the associative
    // array $params.  For example, it could be:
    //
    //  .add_sub(['menu_text' => 'This Item', 'handler' => [$this,'func'])
    //
    elseif (gettype($arg1) == 'array') {
      $params = $arg1;
    }

    if (!array_key_exists('parent_id', $params)) {
      $params['parent_id'] = $this->unique_id;
    }

    // In this case, inferring the menu text is much harder (or impossible).
    // Because of this, we'll simply enumerate the sub sections.  The result
    // will be that the first submenu added (without menu_text) will be
    // called 'Section 1' and so on.  If only the 4th item has no menu_text,
    // then that item will be named 'Section 4'.  Everything else can be
    // reasonably inferred from this.
    if (!array_key_exists('menu_text', $params)) {
      $num = sizeof($this->submenu) + 1;
      $params['menu_text'] = "Section $num";
    }

    // Finally, add this submenu item to the list.
    array_push(
      $this->submenu,
      new MenuItem($this->M, $params)
    );

    // If this item has a parent, return the parent; otherwise, return this.
    // The reason is that if there's no parent, this is a top-level menu
    // item.  If there is a parent, then we want the next added sub to be a
    // peer of this one.
    return ($this->parent) ? $this->parent : $this;
  }

  // This returns a submenu of the current menu.  If there are none, it
  // returns null.  If the name (display name) is not specified, it returns
  // the last one in the list (the most recently appended).
  function submenu ($name=NULL) {
    // If there are no submenus, return null.
    if (sizeof($this->submenu) < 1) {
      return NULL;
    }
    // If there are submenus, but no name specified, return the last one in
    // the list.
    if (!$name) {
      $sub = array_pop($this->submenu);
      array_push($this->submenu, $sub);
      return $sub;
    }
    // Finally, if we got a name, return the matching one (or null if there
    // isn't one that matches).
    else {
      foreach ($this->submenu as $sub) {
        if ($sub->menu_text == $name) {
          return $sub;
        }
      }
    }

    return NULL;
  }

  function render () {
    // First we render the menu item, then any submenu items.  Easy as pork.
    // 
    // If this has no parent, enter it as a top-level menu item.
    if (!$this->parent_id) {
      add_menu_page(
        $this->page_title,
        $this->menu_text,
        $this->permissions,
        $this->unique_id,
        $this->handler
      );
    }
    // Otherwise, it's a submenu item.
    else {
      $_handler = $this->handler;
      add_submenu_page(
        $this->parent_id,
        $this->page_title,
        $this->menu_text,
        $this->permissions,
        $this->unique_id,
        function () {
          echo "<div class='wrap'>";
          call_user_func($this->handler);
          echo "</div>";
        }
      );
    }

    // Next, we loop through the submenu items and add them.
    foreach ($this->submenu as $sub) {
      $sub->render();
    }
  }

  private function _set_property_from_array ($property, $array) {
    // If it's in array, it gets into $this.  If it's not, then $this->...
    // remains unset.  This is desirable rather than having some default
    // value here, especially because not everything gets a default value
    // (like ->parent_id).
    if (array_key_exists($property, $array)) {
      $this->$property = $array[$property];
    }
  }

  private function _infer_missing_properties () {
    // Our first and most critical property is the menu text.  This is the
    // visible thing that we can't really easily guess on formatting.  If
    // the plugin name is set to, say, 'Shiny Nickel', then the menu here
    // will be named 'Shiny Nickel'.
    if (!$this->menu_text) {
      $this->menu_text = $this->M->path->name;
    }

    if (!$this->page_title) {
      $this->page_title = $this->menu_text . ' Dashboard Page';
    }

    if (!$this->permissions) {
      $this->permissions = 'manage_options';
    }

    // In our example, the plugin in named 'Shiny Nickel'.  In that case,
    // this would get set to 'shiny_nickel'.
    if (!$this->unique_id) {
      $this->unique_id = strtolower(
        str_replace(' ', '_', $this->parent_id.'_'.$this->menu_text)
      );
    }

    // Man, you gotta have a handler...!
    if (!$this->handler) {
      $this->handler = function () {
        echo <<<EOHTML
        <div class='wrap'>
          Whoever set up this page didn't provide a handler.
        </div>
EOHTML;
      };
    }

    if (!is_callable($this->handler)) {
      $handler_name = $this->handler;
      $this->handler = function (...$args) {
        $jsonargs = json_encode($args);
        echo <<<EOHTML
        <h1>Something went ... right(-ish)!</h1>
        <div class='wrap'>
$jsonargs
          More content here.
        <div class='details'>
<span class='switch'><a href="#">More details...</a></span>
<span class='details'>The provided handler was <code>$handler_name</code></span>
        </div>
EOHTML;
      };
    }

  }


}

?>
