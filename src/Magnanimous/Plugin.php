<?php
namespace Magnanimous;

class Plugin {
  public $path;  // The base path (absolute) to the plugin dir.
  public $base;  // The main file of the plugin (it's entry point).
  public $name;  // A well-formatted, displayable name of the plugin.

  private $M;

  function __construct ($M, $params=[]) {
    $this->M = $M;

    if (array_key_exists('base', $params)) {
      $this->base = realpath($params['base']);
    }

    if (array_key_exists('path', $params)) {
      $this->path = realpath($params['path']) . '/';
    } elseif ($this->base) {
      $this->path = pathinfo($this->base)['dirname'] . '/';
    }

    if (array_key_exists('name', $params)) {
      $this->name = $params['name'];
    } else {
      $this->name = 'Your plugin needs a Magnanimous name!';
    }

  }

  // This always return 
  function url_base () {
    return plugin_dir_url($this->base);
  }

  // This always return 
  function disk_base () {
    return $this->path;
  }

  // This assumes a path relative to the plugin base path.
  function path_to($fn) {
    $this->M->file->absolute_path($fn);
  }
}

?>
