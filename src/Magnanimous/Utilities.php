<?php
namespace Magnanimous;

require_once('Str.php');
require_once('File.php');

class Utilities {

  // Why start at 100?  Who are you, the start number police?  I won't be a
  // part of your cryptofacist regime!
  public static $_counter = 100;

  public $str;
  public $string;
  public $file;

  private $M;

  function __construct ($M) {
    $this->M = $M;

    // These are aliases for the same thing.
    $this->string = new Str($M);
    $this->str    = $this->string;

    $this->file   = new File($M);
  }

  // So, apparently the php builtin 'uniqid' doesn't garauntee uniqueness.
  // We fix that here (for ids generated through the Magnanimous object) by
  // just inserting a number that increments each time a unique id is
  // created.  I'm not 100% on the php internals, but I believe that because
  // the $_counter is static, we get thread-safe uniquness.
  //
  // Note that all IDs created here start with 'M'.  This is for
  // compatibility with CSS/HTML identifiers which shouldn't start with
  // digits.
  //
  // If you really want another prefix, you can put one on (after the M),
  // but it's pretty unnecessary, generally.
  //
  // Example usage:
  //
  //    $this->M->util->unique_id();        // M275586400dc35492
  //    $this->M->util->unique_id('_foo_'); // M_foo_275586400dc35492
  //
  function unique_id ($prefix='') {
    return uniqid("M$prefix" . Utilities::$_counter++);

  }

  // This internal function lets Magnanimous do things like create a unique
  // ID based on the plugin's call location.
  function _consistent_caller_id () {
    $stack = debug_backtrace();

    $target = NULL;
    while (!$target) {
      $node = array_shift($stack);
      if (substr($node['class'], 0, strlen('Magnanimous')) !== 'Magnanimous') {
        $target = $node;
      }
    }

    // Now $target is the first (most recent) stack node that doesn't come
    // from Magnanimous.  There are various kinds of node in the stack, so
    // we detect and hash.
    if ( array_key_exists('class'    , $target)
      && array_key_exists('file'     , $target)
      && array_key_exists('function' , $target)
      && array_key_exists('line'     , $target)
    ) {
      return
          $target['class']
        . $target['file']
        . $target['function']
        . $target['line']
      ;
    }

    else if (
         array_key_exists('class'    , $target)
      && array_key_exists('object'   , $target)
      && array_key_exists('function' , $target)
      && array_key_exists('type'     , $target)
      && array_key_exists('args'     , $target)
    ) {
      return
          $target['class']
        . $target['function']
        . json_encode($target['object'])
        . $target['type']
        . json_encode($target['args'])
      ;
    }
  }

}



?>
