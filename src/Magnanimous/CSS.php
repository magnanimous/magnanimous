<?php
namespace Magnanimous;

class CSS {
  private $M;

  function __construct ($M) {
    $this->M = $M;
  }

  // ******************************************************
  //
  //  CSS Management
  //

  // This is the main entry point for registering styles.  Just pass in a
  // list of names and dependencies.  In some future version, I may get
  // dependencies by reading the file from disk and scanning for deps (like
  // in comments in a header or something).  For now, this is how it is.
  //
  // Usage Example
  //
  //    $this->M->css->register([
  //      'MyClass'      => ['dependency'],
  //      'MyOtherClass' => ['dependency', 'MyClass'],
  //    ]);
  //
  // This will load three files:
  //
  //    ./css/dependency.css
  //    ./css/MyClass.css
  //    ./css/MyOtherClass.css
  //
  // I would recommend doing this all in one file.  For example, you could
  // have a php file called 'css.php' with this content:
  //
  //    ./javastyles.php
  //    --
  //    $this->M->css->register([
  //      'MyClass'      => ['dependency'],
  //      'MyOtherClass' => ['dependency', 'MyClass'],
  //    ]);
  //
  // That's it.  Now in your plugin functions, you can just require things:
  //
  //    $this->M->css->requires('MyAwesomeStuff');
  //
  // Now all the dependencies get loaded, etc.
  function register ($map) {
    foreach ($map as $name => $deps) {
      $this->register_file($name, $deps);
    }
  }

  // You probably don't want this.  I left it public in case someone really
  // feels a need to put the style into the footer or change the version.
  // Originally it was private, but I figured, hey, let's give people
  // options.  Really, though, you probably want ->register().
  //
  // In the future, I'll probably deprecate this and have the version be
  // auto-read out of the file by file name or something.  You've been
  // warned! =)
  function register_file (
    $name,
    $deps_arr=array(),
    $version=false,
    $media=false
  ) {
    $url = $this->_full_or_minified_url($name);

    // Now slap it into WordPress's css registry.
    wp_register_style($name, $url, $deps_arr, $version, $media);
  }

  // An opinionated wrapper around wp_enqueue_style
  function requires (
    $name,
    $deps_arr=array(),
    $version=false,
    $media=false
  ) {
    // If we didn't get an explicit version, create one from the file's last
    // modified time.
    if (!$version) {
      $version = $this->_default_file_version(
        $this->_full_or_minified_path($name)
      );
    }

    // Get the style url.  Use a minified version if there is one.
    $url = $this->_full_or_minified_url($name);

    // Now pass it off to WordPress.
    wp_enqueue_style(
      $name,
      $url,
      $deps_arr,
      $version,
      $media
    );
  }

  // #Thanks: Andrija Naglic
  //
  // This returns a consistent "version" for a file based on its
  // modification time.  For example, a file last modified on 
  // 2016-05-22 12:39:44 will have the version:  
  //
  //    160522.123944
  //
  // If you then work on the file and save it a few minutes later, the new
  // version will be, say, 
  //
  //    160522.124351
  //
  // Once the plugin is installed, it will have a fixed version until a new
  // one overwrites it, so site visitors get a cached version.
  //
  // This is a good default, but feel free to use something like Semantic
  // Versioning for project development, of course.
  private function _default_file_version ($fn) {
    return date('ymd.Gis', filemtime($fn));
  }

  private function _full_or_minified_path ($basename) {
    // By default, prefer the basename with a .css suffix.
    $fn = "$basename.css";

    // If there's a minified version of the style, use that instead.
    if (file_exists($this->M->path->path . "resources/css/$basename.min.css")) {
      $fn = "$name.min.css";
    }

    return $this->M->path->path . "resources/css/$fn";
  }

  private function _full_or_minified_url ($basename) {
    // By default, prefer the basename with a .css suffix.
    $fn = "$basename.css";
    // If there's a minified version of the style, use that instead.
    if (file_exists($this->M->path->path . "resources/css/$basename.min.css")) {
      $fn = "$name.css";
    }
    return plugin_dir_url($this->M->path->path . "resources/css/") . "css/$fn";
  }

  // ******************************************************
  //
  //  Style Injection
  //
  
  // This is available as a compatiblity consideration.  It's probably never
  // really a good idea.  But hey, you're your own man.

  // Usage Example:
  //
  //    Say you have a file, my-widget.css with the following content:
  //
  //      /* This awesome design turns everything red. */
  //      html, body, * {
  //        background-color: red;
  //        color: red;
  //        border-color: red;
  //        box-shadow: 1px 1px 1px red;
  //      }
  //
  //    It doesn't need to be registered or enqueued with WordPress.
  //
  //    Now, in your PHP plugin file, you can do this:
  //
  //      echo $this->M->css->inject('my-widget');
  //
  //    That's it.  You get a one-off style installed.  Note that ->inject
  //    returns a string, but doesn't actually do the printing to the page.
  //    That's a good thing. =)
  //
  function inject ($name, $options=[]) {
    $style = file_get_contents($this->_full_or_minified_path($name));

    // If there were any requirements, load 'em up.
    if (array_key_exists('require', $options)) {
      foreach ($options['require'] as $req) {
        $this->M->css->requires($req);
      }
    }

    return "<style type='text/css'>$style</style>";
  }
}

?>
