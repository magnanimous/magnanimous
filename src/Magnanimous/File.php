<?php
namespace Magnanimous;

class File {

  private $memoize = [];
  private $M;

  function __construct ($M) {
    $this->M = $M;
  }

  // $M->html->read('subscription_list');
  //
  // Reads a file from disk and puts it into memory.
  function read ($filename, $basepath = null) {
    if (!$basepath) {
      $basepath = $this->M->path->path;
    }

    if (isset($this->memoize[$filename])) {
      return $this->memoize[$filename];
    } else {
      $this->memoize[$filename] = 
        file_get_contents($basepath . $filename);
      return $this->memoize[$filename];
    }
  }

}

?>
