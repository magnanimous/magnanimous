<?php
namespace Magnanimous;

require_once 'Debug/Console.php';

// Documentation Notes
//
// * You probably need to create your debug file and chown them to
//   www-data:www-data in order for them to be writable.
//
// * On linux, try `tail -f /tmp/magnanimous.log` to always see your window.
//
// * now there's m-tools.sh.  Try ./m-tools.sh notify & to get a background
//	 process running that sends console information to Ubuntu's notify-send
//	 service.
//
// * Putting the log files into the plugin directory may be preferred to
//   putting them in /tmp/, but there's that permissions error...  Maybe
//   later we'll add am option.
class Debug {
	private $M;
	public $console;

	// By default, they can all share a log file.
	public $log_file   = 'magnanimous.log';
	public $debug_file = 'magnanimous.log';
	public $info_file  = 'magnanimous.log';
	public $warn_file  = 'magnanimous.log';
	public $error_file = 'magnanimous.log';

	public $log_path = '/tmp/';

	function __construct ($M, $params=[]) {
		$this->M = $M;
		$this->console = new Debug\Console($M);
	}

	function serialize ($data) {
		if (gettype($data) == 'string') {
			return $data;
		} else {
			return json_encode($data);
		}
	}

  // This prints a warning to the console that the calling method is a stub
  // and has not been implemented correctly.  It's a convenience function
  // for the developer so he can basically turn on a to-do checklist.
  function mark_stub ($name='unknown') {
    $this->console->warn(
      'Method stub not implemented:', 
      debug_backtrace()[1]['class'] . '->' . debug_backtrace()[1]['function']
    );
  }

	function emit ($type, ...$args) {
		// Maybe we'll put in something like this some time.
		// date_default_timezone_set('Australia/Melbourne');
		$output = date('Y/m/d h:i:s'); 
		$output .= " [" . strtoupper($type) . "] ";

		foreach ($args as $arg) {
			$output .= $this->serialize($arg) . ' ';
		}

		$fn_type = "${type}_file";
		//    $fn = $this->M->path->path . $this->$fn_type;
		$fn = $this->log_path . $this->$fn_type;
		file_put_contents($fn, "$output\n", FILE_APPEND | LOCK_EX);
	}

	function debug (...$args) { $this->emit ('debug' , ...$args); }
	function log   (...$args) { $this->emit ('log'   , ...$args); }
	function info  (...$args) { $this->emit ('info'  , ...$args); }
	function warn  (...$args) { $this->emit ('warn'  , ...$args); }
	function error (...$args) { $this->emit ('error' , ...$args); }
}

?>
