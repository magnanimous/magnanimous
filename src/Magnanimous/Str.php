<?php
namespace Magnanimous;

class Str {

  private $M;

  function __construct ($M) {
    $this->M = $M;
  }

  // ******************************************************
  //
  //  String Manipulation
  //

  // prepare($str[, ...$args]);
  //
  // A light version of the POSIX standard (s)printf.  This doesn't do any
  // type recognition or padding or anything else, it simply replaces
  // occurrences of '%s' with ordered arguments.
  //
  // Note that this *will* get tripped up if the new replacement string
  // contains '%s' (unless you're doing it on purpose, you magnificent
  // bastard!).
  //
  // Example Usage:
  //
  //    You have a template string:
  //
  //      $mystr = 'I am the %s man, I am the %s man, I am the %s!';
  //      prepare($mystr, 'egg', 'egg', 'Walrus');
  //      // Output:  'I am the egg man, I am the egg man, I am the Walrus!'
  //
  function prepare ($str, ...$args) {
    $token = '%s';

    foreach ($args as $arg) {
      // If this argument is an associative array, treat it specially.
      if (gettype($arg) == "array") {
        $str = $this->_replace_with_array($str, $arg);
      }
      // If not, just do string replacement.
      else {
        $position = strpos($str, $token);
        if ($position !== false) {
          $str = substr_replace($str, $arg, $position, strlen($token));
        }
      }
    }

    return $str;
  }

  private function _replace_with_array ($str, $arr) {
    foreach ($arr as $name => $value) {
      $position = true;

      while ($position) {
        $position = strpos($str, $name);
        if ($position !== false) {
          $str = substr_replace($str, $value, $position, strlen($name));
        }
      }
    }

    return $str;
  }

}

?>
