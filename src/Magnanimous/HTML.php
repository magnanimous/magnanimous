<?php
namespace Magnanimous;

class HTML {

  private $memoize = [];
  private $M;

  function __construct ($M) {
    $this->M = $M;
  }

  // $M->html->read('subscription_list');
  //
  // Reads a file from disk and puts it into memory.  Memoizes on the way.
  function file ($name) {
    $content = $this->M->util->file->read("resources/html/$name.html");
    $content = preg_replace('/\r|\n/', ' ', $content);
    $content = preg_replace('/\s\s+/', ' ', $content);
    $content = preg_replace('/<!--.*?-->/', '', $content);
    return $content;
  }

  // This is a thin wrapper around the Magnanimous\Str ->prepare method.
  function prepare ($str, ...$args) {
    return $this->M->util->str->prepare($str, ...$args);
  }

  function prepare_file ($filename, ...$args) {
    return $this->prepare($this->file($filename), ...$args);
  }

  function prepare_files (...$filenames) {
    $output = [];
    foreach ($filenames as $filename) {
      array_push($output, $this->prepare_file($filename));
    }
    return $output;
  }

  function template ($filename, ...$args) {
    return $this->prepare_file("$filename.template", ...$args);
  }

  function templates ($arr) {
    $output = [];

    foreach ($arr as $filename) {
      $output[$filename] = $this->template($filename);
    }

    return $output;
  }

}

?>
