<?php
namespace Magnanimous;

class Forms {

  private $handler_lookup = [];
  private $M;

  function __construct ($M) {
    $this->M = $M;
    $this->token = 'archon';
//    add_action('admin_menu', [$this, '_interpret_action'], 1);
  }

  function set_action ($name, $handler) {
    if ($name && is_callable($handler)) {
      $this->handler_lookup[$name] = $handler;
    }

    return "<input type='hidden' name='$this->token' value='$name'/>";
  }

  // This works by passing in some HTML and a php-side handler (and some
  // build parameters).  The HTML gets wrapped in a form and some magical 
  // inputs are put in to direct the form correctly.  
  //
  //
  /**
   * Create a form and associate a php handler with it.
   *
   * @param string $html A string of html, probably with inputs, buttons,
   * and other form elements.
   *
   * @param callable $handler A callable to be called when the form is
   * submitted.
   *
   * @param array $params An array map of options for form creation.
   * Available options include
   *
   */
  function create ($name, $html, $handler, $params=[]) {
    $name = preg_replace('/\W+/', '_', $name);

    //-----------
    // This is the key.  We actually do processing of a form we're creating
    // immediately after registering it but before rendering it.  This means
    // if we've submitted the form, we have ready data and can do various
    // kinds of feedback.
    //-----------
    if ($name && $html && is_callable($handler)) {
      $this->handler_lookup[$name] = $handler;
      $results = $this->_interpret_action($name);
    } elseif ($name && $html && !is_callable($handler)) {
      $this->M->debug->console->log($name);
      $handler = [$this, '_default_form_handler'];
      $this->handler_lookup[$name] = $handler;
      $results = $this->_interpret_action($name);
    }
    //-----------
    
    // The results of the form processing are used to modify the form.  This
    // is a way passively updating the form with anything new.  The value
    // here is that a form handler can update a field by returning a map of
    // replacements like this:
    //    [ "value='red'", "value='blue'"]
    if (gettype($results) == "array") {
      $html = $this->M->util->string->prepare($html, $results);
    }

    $token = "<input type='hidden' name='M_token' value='$name'/>";

    return <<<EOFORM
      <form method='POST' action=''>
        $token
        $html
      </form>
EOFORM;
  }

  function _default_form_handler () {
    echo "This form doesn't have a handler assigned.";
  }

  function _interpret_action ($name) {
    if (isset($_POST['M_token'])
    && array_key_exists($_POST['M_token'], $this->handler_lookup)
    && $_POST['M_token'] == $name) {
      return call_user_func($this->handler_lookup[$_POST['M_token']]);
    }

    return NULL;
  }

  function from_file ($filename, $handler, ...$args) {
    $name = $this->M->util->_consistent_caller_id();

    $form = $this->create(
      $name,
      $this->M->html->prepare_file($filename, ...$args),
      $handler
    );

    return $form;
  }

}

?>
