<?php
namespace Magnanimous;

class Path {

  private $M;
  private $source;

  public $path;
  public $base;
  public $name;

  function __construct ($source) {
    $this->reset_to($source);
  }

  function reset_to ($source) {
    if (!$source) {
      return false;
    }

    $this->source = $source;
    $this->path   = $source->path;
    $this->base   = $source->base;
    $this->name   = $source->name;

    return true;
  }

  function url_base () {
    return $this->source->url_base();
  }

  function disk_base () {
    return $this->source->disk_base();
  }

  function path_to ($filename) {
    return $this->source->path_to($filename);
  }
}

?>
