<?php
namespace Magnanimous\JS;

require_once 'AJAX/Response.php';

class AJAX {
  private $M;
  private static $ajax_handlers;

  public $ajax_url;
  public $ajax_action;

  function __construct ($M, $params=[]) {

    $this->M = $M;

    $this->ajax_url      = admin_url('admin-ajax.php');
    AJAX::$ajax_handlers = [];

    // Setting the ajax_action this way lets us have uniqueness to each plugin.
    $ajax_action = strtolower($this->M->path->name);
    $ajax_action = preg_replace("/[^0-9a-zA-Z ]/m", "", $ajax_action);
    $ajax_action = preg_replace("/ /", "_", $ajax_action);
    $this->ajax_action = "${ajax_action}_ajax";
  }

  /**
   * Handle AJAX calls.  This is an instance-based central handler for AJAX
   * calls.
   */
  function _ajax_handler () {
    try {
      if (!array_key_exists('plugin_action', $_POST)) {
        echo 0;

        if ($this->M->debug) {
          $this->_json_response([
            'error' => 'Data was provided, but no plugin_action was specified.'
          ]);
        }

        wp_die();
      }

      $action = $_POST['plugin_action'];

      if (!array_key_exists($action, AJAX::$ajax_handlers)) {
        echo json_encode([
          'error' => "The requested ajax handler does not exist:  $action"
        ]);

        if ($this->M->debug) {
          $this->_json_response([
            'error' => "No handler specified for this action ($action).",
            'handlers' => json_encode(AJAX::$ajax_handlers)
          ]);
        }
      }

      // If the handler isn't callable for some reason, give a default
      // response.
      else if (!is_callable(AJAX::$ajax_handlers[$action])) {
        $this->default_ajax_handler(
          ['error' => "The handler for '$action' is not callable."]
        );
      }

      // Finally, call it.
      else {
        // Call the callable.
        call_user_func(
          AJAX::$ajax_handlers[$action], 
          $_POST['data']
        );
      }

      wp_die();
    }

    catch (exception $e) {
    }

    finally {
      wp_die();
    }
  }

  /**
   * This only needs to happen once (per plugin).  It tells Wordpress that
   * this plugin is receiving ajax calls and provides the _ajax_handler as a
   * callable.
   */
  function _register_with_wordpress () {
    $ajax_action = $this->ajax_action;
    add_action("wp_ajax_$ajax_action"       , [$this, '_ajax_handler']);
    add_action("wp_ajax_nopriv_$ajax_action", [$this, '_ajax_handler']);
  }

  function _json_response ($data=false) {
    if (!$data) {
      $data = ['error' => 'An unknown error has occurred.'];
    }

    echo json_encode($data);
  }

  // Register new AJAX handlers.  
  function _register_ajax_handler ($name, $handler) {
    if (!is_callable($handler)) {
      $this->M->debug->log(
        ['error' => "The specified ajax handler (for $name) is not callable."]
      );
    } else {
      AJAX::$ajax_handlers[$name] = $handler;
    }
    return true;
  }

  // Ex: ...->gen_handlers($this, [
  //  'do-something',
  //  'do-something-else',
  //  'handle_something'
  // ]);
  //
  // This produces handlers like:
  //
  //  ...('do-something',      [$obj, '_ajax_do_something']);
  //  ...('do-something-else', [$obj, '_ajax_do_something_else']);
  //  ...('handle-something',  [$obj, '_ajax_handle_something']);
  //
  function gen_handlers ($obj, $names) {
    foreach ($names as $name) {
      $function_name = '_ajax_' . str_replace('-', '_', $name);
      $this->_register_ajax_handler($name, [$obj, $function_name]);
    }
  }

}

?>
