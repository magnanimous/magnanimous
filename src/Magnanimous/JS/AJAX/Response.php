<?php
namespace Magnanimous\JS\AJAX;

class Response {
  private $M;

  public $status;
  public $data;

  function __construct ($M, $params=[]) {
    $this->M = $M;
    $this->status = "Error";
    $this->data = [];
  }

  function render () {
    return json_encode($this->data);
  }
}

?>
