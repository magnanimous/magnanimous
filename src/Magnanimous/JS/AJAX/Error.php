<?php
namespace Magnanimous\JS\AJAX;

class Error {
  public $type;
  public $info;
  public $data;

  function __construct (
    $type="Unknown",
    $info="An unknown error has occurred."
  ) {
    $this->type = $type;
    $this->info = $info;
    $this->data = null;
  }
}

?>
