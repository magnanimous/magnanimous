<?php
namespace Magnanimous\JS\AJAX;

class Request {
  public $plugin;
  public $action;
  public $data;
  private $user;

  function __construct ($params=[]) {
    $this->data = $params['data'];
    $this->plugin = str_replace('_ajax', '', $params['action']);
    $this->action = $params['plugin_action'];
    $this->user = wp_get_current_user();
  }

  function __get ($property) {
    if ($property == 'user') {
      return $this->user;
    }

    return null;
  }

  function has_data ($list=[]) {
    $results = [
      'isValid'  => true,
      'missing'  => [],
      'mistyped' => [],
      'debug'    => [],
    ];

    foreach ($list as $name => $types) {
      // If the $name is an integer, then this item was simply enumerated by
      // name.  In that case, we only need to check whether or not the field
      // is present.  This is equivalent to:
      //
      //    'field_name' => ['required']
      //
      if (is_int($name)) {
        if (!array_key_exists($types, $this->data)) {
          array_push($results['missing'], $types);
        }
      } 
      
      // On the other hand, if the $name is not an int, then we got a pair
      // like this:  
      //
      //    ['field_name' => ['str', 'int', 'null']]
      //
      // In this case, that's interpreted as the following statement:
      //
      //    field_name is a specified input.  If included, it must be of
      //    types str or int or null (which includes missing completely).
      //
      //  Leaving off null makes the field required implicitly.  It can also
      //  be made explicit using 'required'.
      //
      else {
        // $name => $types is, for example, 'field_name' => [...]

        // If it's NOT marked null and the field is just plain missing, mark
        // it as missing.
        if (
          !in_array('null', $types) &&
          !array_key_exists($name, $this->data)
        ) {
          array_push($results['missing'], $name);
        }
       
        // Also, it could be missing but WITH null.  filter that here.
        else if (!array_key_exists($name, $this->data)) {
          // we don't need to do anything for this.  It's just not there,
          // but that's permissible in this case.
        }

        // Everything else is *required* to exist and required to be of
        // certain types.
        //
        //    'field_name' => ['int', 'str', 'null']
        //
        // In order for this to work, it has to match ANY ONE of the types,
        // OR be missing, but it cannot exist as a different type.
        //
        // Here are the valid types:
        //   "boolean"
        //   "integer"
        //   "double"
        //   "string"
        //   "array"
        //   "object"
        //   "resource"
        //   "resource (closed)"
        //   "NULL"
        //   "unknown type"
        else {
          $dataType = gettype($this->data[$name]);

          $results['debug'][$name] = $dataType;

          // If the type of this data is in the whitelist of types, then
          // we're good to go.
          if (!in_array($dataType, $types)) {
            $results['mistyped'][$name] = [
              'provided' => $dataType,
              'allowed' => $types
            ];
          }
				}
      }
    }

    if (sizeof($results['missing']) > 0 || sizeof($results['mistyped']) > 0) {
      $results['isValid'] = false;
    }

    return $results;
  }
}

?>
