<?php
namespace Magnanimous;

require_once 'Module/Manifest.php';
require_once 'Module/Instruction.php';
require_once 'Module/HTML.php';
require_once 'File.php';

/***
 * The core concept of a Magnanimous Module is somewhat akin to a "react
 * app".  You bundle HTML, CSS, JS, SQL, and anything else into a single
 * directory.  Resources are accessed the same way, but you create an
 * instance of a module and slip it into place
 *
 * Here's some dreamcode:
 *
 * // Ideally a module should be able to reference its own components.  You
 * // load the module, then 
 *
 * // Doing this lets us create the module with its own internal reference
 * to the Magnanimous object.
 * $module = $M->create_module('smooth-criminal');
 *
 * $module = new Module(...);
 * echo $module->render([
 *  'rerender' => true (default: false)
 * ]);
 *
 */

class Module {

  private $M;
  private $cachedRender;

  public  $manifest;
  public  $relative_module_path;
  public  $name;
  // Data exists as a pseudo-global for each script.  Each script gets its
  // own copy.
  public  $data;

  function __construct ($M, $params) {
    $this->M = $M;

    // I need to check whether this exists first, and send out an error if
    // not.  Or maybe a default module (in Magnanimous) that has effectively
    // a 404 error for modules and renders as a nice error message.
    $this->name = $params['name'];

    $this->relative_module_path = 'modules/' . $this->name . '/';

    $this->manifest = new Module\Manifest(
      $M, $this->relative_module_path
    );

    $this->manifest->read_file();
  }

  // The idea is something like a module named
  //   "My Awesome -- Module!!!"
  // becomes
  //   my-awesome-module
  function name_to_dir ($name) {
    $str = preg_replace('/\W+/', '-', $name);
    $str = preg_replace('/^-+/', '' , $str);
    $str = preg_replace('/-+$/', '' , $str);
    $str = preg_replace('/-+/' , '-', $str);
    return $str;
  }

  function disk_path () {
    return $this->M->path->disk_base() . $this->relative_module_path;
  }

  // Usage example:
  //
  // resource_path("js", "bisuit-butter");
  //
  // produces, for example:
  //  /blah/blah/modules/my-module/pub/js/biscuit-butter.js
  //
  // This information is found by interrogating the manifest.
  //
  function resource_path ($type, $name) {
    return $this->manifest->resource_path($type, $name);
  }

  function resource_url ($type, $name) {
    return $this->manifest->resource_url($type, $name);
  }

  // $module->render();
  // $module->render(['refresh' => true]);
  function render ($params=[]) {
    // If we're rendering, we want to make sure Magnanimous is available
    // browser-side.
    $this->M->js->load_Magnanimous();

    // If we got some data in, use it.  Otherwise, use null.
    $data = null;
    if (array_key_exists('data', $params)) {
      $data = $params['data'];
    }

    // We don't want to use the cached version if data is passed in because
    // it could affect the rendering.
    if ($this->cachedRender && !$params['refresh'] && !$data) {
      return $this->cachedRender;
    }

    $this->cachedRender = $this->prepare_entrypoint_file(['data' => $data]);
    return $this->cachedRender;
  }

  // Given a file name (with full path), read the file and prepare it by
  // replacing what needs replacing.
  private function prepare_entrypoint_file($params=[]) {
    return $this->prepare_file(
      $this->disk_path() . $this->manifest->get_entrypoint(),
      $params
    );
  }

  // When preparing a file, we need to read it then find any XML processing
  // instructions using <?include ... ? > and get the tags out.  Here's an
  // example:
  //
  //    // Include the requested file directly
  //    <?inject type="css" src="main" ? >
  //    <?inject type="js"  src="main" ? >
  //
  //    // Create a <style> tag link.
  //    <?link type="css" src="main" ? >
  //
  //    // Create a <script> tag link.
  //    <?link type="js"  src="main" ? >
  //
  // There's not supposed to be a space between the ? and >, but it breaks
  // my vim highlighting.
  // 
  // If we want, we can have more instructions than <?include, such as
  // <?link if we want to just link a CSS directly.
  //
  private function prepare_file ($fn, $params=[]) {
    if (!file_exists($fn)) {
      $pattern = "|.*" . $this->relative_module_path . '|';
      $trimmed_fn = "&lt;module_path&gt;/" . preg_replace($pattern, "", $fn); 
      return "The requested file cannot be found:  ($trimmed_fn)";
    }

    $contents = file_get_contents($fn);

    // Re-base all relative links
    $contents = Module\HTML\rebaseHtmlUrls(
      $contents,
      $this->manifest->getModuleDistributionUrl(),
      [
        'relative' => true,
        'absolute' => true,
      ]
    );

    $contents .= $this->manifest->getModuleDistributionUrl();

    // Instructions becomes an associative array of Module\Instruction
    // objects keyed to the instruction string.  For example:
    //
    // [
    //    '<?link type="js"  src="main" ? >' => $instruction_obj
    // ]
    $instructions = Module\Instruction::from_string(
      $contents, $this, $this->M, $params
    );

    // e.g. '<?link type="js"  src="main" ? >' => $instruction_obj
    foreach ($instructions as $htmlstr=>$instruction) {
      $contents = str_replace($htmlstr, $instruction->render_string, $contents);
    }

    // Before we're done, let's make sure we have Magnanimous available in
    // the browser.

    return $contents;
  }

  private function interp_directive ($instruction) {
    $result = [];
    $attributes = [];

    $directive = $this->extract_directive($instruction);
    if (!$directive) {
      return;
    }

    // Now that we have the directive, let's get a list of the element's
    // attributes.
    $result['directive'] = $directive;

    // This is now an associative array of attributes.
    $result['attributes'] = $this->extract_attributes($instruction);

    // **********************
    // Now actually render it however it needs to be rendered.
    //
    // If it's a link, we just get the link and include it directly.  If
    // it's an include, read the file and return it.

    return $result;
  }

  private function extract_directive ($instruction) {
    $valid_instructions = "(include|link)";
		$pattern = "#<\?($valid_instructions)\s.*\?>#si";

		preg_match($pattern, $instruction, $matches);
    if ($matches && $matches[1]) {
      return $matches[1];
    } else {
      return;
    }
  }

  // php> $txt = '<?include foo="bar" baz=\'bof\'? >';
  // php> $pattern = '#(\w*)=((?:\'|").*?(?:\'|"))#si';
  // php> preg_match_all($pattern, $txt, $matches);
  // php> print_r($matches);
  // Array (
  //     [0] => Array
  //         (
  //             [0] => foo="bar"
  //             [1] => baz='bof'
  //         )
  // 
  //     [1] => Array
  //         (
  //             [0] => foo
  //             [1] => baz
  //         )
  // 
  //     [2] => Array
  //         (
  //             [0] => "bar"
  //             [1] => 'bof'
  //         )
  // )
  private function extract_attributes ($instruction) {
		$pattern = '#(\w*)=((?:\'|").*?(?:\'|"))#si';
    preg_match_all($pattern, $instruction, $matches);

    if ($matches && $matches[0]) {
      $results = [];
      foreach ($matches[0] as $match) {
        $parts = explode('=', $match, 2);
        $parts[1] = preg_replace('/^["\']/si', "", $parts[1]);
        $parts[1] = preg_replace('/["\']$/si', "", $parts[1]);
        $results[$parts[0]] = $parts[1];
      }
      return $results;
    } else {
      return [];
    }
  }

  /****
   *
   *  $foo = <<<EOP
   *  <?include
   *    type="css"
   *    src="bar"
   *  ?> 
   *    This is the <em>thing</em> here.
   *    
   *  <?include
   *    type="js"
   *    src="baz"
   *  ?> 
   *  EOP;
   *  
   *  preg_match_all("#(<\?include.*?>)#si", $foo, $out);
   *  echo json_encode($out,JSON_PRETTY_PRINT);
   *
   */
}

?>
