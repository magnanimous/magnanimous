<?php
namespace Magnanimous;

class Theme {

  public $path;  // The base path (absolute) to the theme dir.
  public $base;  // The main file of the theme (it's entry point).
  public $name;  // A well-formatted, displayable name of the theme.

  public $url;   // The URL path.
  public $disk;  // 

  private $M;

  function __construct ($M, $params=[]) {
    $this->M = $M;

    if (array_key_exists('base', $params)) {
      $this->base = realpath($params['base']);
    }

    if (array_key_exists('path', $params)) {
      $this->path = realpath($params['path']) . '/';
    } elseif ($this->base) {
      $this->path = pathinfo($this->base)['dirname'] . '/';
    }

    if (array_key_exists('name', $params)) {
      $this->name = $params['name'];
    } else {
      $this->name = 'Your theme needs a Magnanimous name!';
    }

  }

  function url_base () {
    return get_stylesheet_directory_uri() . '/';
  }

  function disk_base () {
    return $this->path;
  }

  // This assumes a path relative to the theme base path.
  function path_to($fn) {
    $this->M->file->absolute_path($fn);
  }
}

?>
