<?php
namespace Magnanimous;

require_once 'UI/Dashboard.php';

class UI {
  private $M;
  public $dashboard;

  function __construct ($M, $params=[]) {
    $this->M = $M;
    $this->dashboard = new UI\Dashboard($M);
  }
}

?>
