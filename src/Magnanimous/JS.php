<?php
namespace Magnanimous;

require_once 'JS/AJAX.php';

class JS {
  private $M;
  public $ajax;

  function __construct ($M) {
    $this->M    = $M;
    $this->ajax = new JS\AJAX($M);
    $this->ajax->_register_with_wordpress();
  }

  // This is a special function that bootstraps the browser side of
  // Magnanimous.  It does it intelligently-- you only get it when you call
  // a function on the server side that needs it.
  function load_Magnanimous () {
    add_action("admin_footer", [$this, '_load_Magnanimous']);
    add_action("wp_footer",    [$this, '_load_Magnanimous']);
  }

  function _load_Magnanimous () {
    // Get the disk path and url to the Magnanimous browser file.
    $MJS_path = $this->M->path_to_resource('js/Magnanimous.js');
    $MJS_url  = plugin_dir_url($MJS_path) . 'Magnanimous.js';

    wp_enqueue_script('jquery');

    wp_register_script('Magnanimous', $MJS_url, [], null, false);
    wp_enqueue_script('Magnanimous', $MJS_url);

    $ajax_action = $this->_get_ajax_handler_name();

    echo $this->M->js->inject_str(
      $this->M->util->str->prepare(
        $this->M->util->file->read('js/create-plugin.js', $this->M->MagnanimousDir),
        $this->M->path->name,
        // Among the params we want to pass in are the ajax url.
        json_encode([
          'ajax_url'    => admin_url('admin-ajax.php'),
          'ajax_action' => $ajax_action
        ])
      ),
      null,
      // Injection options
      [ 'path' => $MJS_path ]
    );

    // Register handlers for this.
    add_action("wp_ajax_nopriv_$ajax_action", [$this, 'handle_ajax']);
    add_action("wp_ajax_$ajax_action"       , [$this, 'handle_ajax']);
  }

  private function _get_ajax_handler_name () {
    // Create our ajax handler name.  If the plugin is named "Shiny Nickel 45",
    // then the result will be "shiny_nickel".
    $ajax_action = strtolower($this->M->path->name);
    $ajax_action = preg_replace("/[^0-9a-zA-Z ]/m", "", $ajax_action);
    $ajax_action = preg_replace("/ /", "_", $ajax_action); 
    return $ajax_action;
  }

  function handle_ajax () {
    echo '{"error": "no data was provided from the browser."}';
    wp_die();

    if (!$_POST['data']) {
      echo '{"error": "no data was provided from the browser."}';
      wp_die();
    }

    $data = $_POST['data'];

    if (!$data['action']) {
      echo json_encode(['error' => 'no action was specified']);
      wp_die();
    }

    if (!$this->ajax_lookup[$data['action']]) {
      $action = $data['action'];
      echo json_encode(
        ['error' => "the specified action ($action) isn't defined"
      ]);
      wp_die();
    } else {
      call_user_func($this->ajax_lookup[$data['action']], $_POST);
      wp_die();
    }

    // WordPress needs all AJAX handlers to die when finished.
    wp_die();
  }

  // ******************************************************
  //
  //  JavaScript Management
  //

  // This is the main entry point for registering scripts.  Just pass in a
  // list of names and dependencies.  In some future version, I may get
  // dependencies by reading the file from disk and scanning for deps (like
  // in comments in a header or something).  For now, this is how it is.
  //
  // Usage Example
  //
  //    $this->M->js->register([
  //      'MyClass'      => ['dependency'],
  //      'MyOtherClass' => ['dependency', 'MyClass'],
  //    ]);
  //
  // This will load three files:
  //
  //    ./js/dependency.js
  //    ./js/MyClass.js
  //    ./js/MyOtherClass.js
  //
  // It will look for the file in the plugin dir .../resources/js/$basename.js
  // By default, it will try to load a minified version first if it finds
  // it, and if not it will load the full version.  Minified versions are
  // signified with the suffix .min.js appended.
  //
  // I would recommend doing this all in one file.  For example, you could
  // have a php file called 'javascripts.php' with this content:
  //
  //    ./javascripts.php
  //    --
  //    $this->M->js->register([
  //      'MyClass'      => ['dependency'],
  //      'MyOtherClass' => ['dependency', 'MyClass'],
  //    ]);
  //
  // That's it.  Now in your plugin functions, you can just require things:
  //
  //    $this->M->js->requires('MyOtherClass');
  //
  // Now all the dependencies get loaded, etc.
  function register ($map) {
    foreach ($map as $name => $deps) {
      $this->register_file($name, $deps);
    }
  }

  // You probably don't want this.  I left it public in case someone really
  // feels a need to put the script into the footer or change the version.
  // Originally it was private, but I figured, hey, let's give people
  // options.  Really, though, you probably want ->register().
  //
  // In the future, I'll probably deprecate this and have the version be
  // auto-read out of the file by file name or something.  You've been
  // warned! =)
  function register_file (
    $name,
    $deps_arr=array(),
    $version=false,
    $in_footer=false
  ) {
    // Assume that we want jQuery as a dependency for every script
    // because... hey, jQuery (good job, Resig).
    $deps = array_merge(['jquery'], $deps_arr);

    $url = $this->_full_or_minified_url($name);

    // Now slap it into WordPress's js registry.
    wp_register_script($name, $url, $deps, $version, $in_footer);
  }

  // An opinionated wrapper around wp_enqueue_script
  function requires (
    $name,
    $deps_arr=array(),
    $version=false,
    $in_footer=false
  ) {
    $deps = array_merge(array('jquery'), $deps_arr);

    // If we didn't get an explicit version, create one from the file's last
    // modified time.
    if (!$version) {
      $version = $this->_default_file_version(
        $this->_full_or_minified_path($name)
      );
    }

    // Get the script url.  Use a minified version if there is one.
    $url = $this->_full_or_minified_url($name);

    // If this isn't a request for Magnanimous and also if Magnanimous isn't
    // loaded, then load it.
    if ($name != 'Magnanimous' && !wp_script_is('Magnanimous', 'enqueued')) {
      $this->load_Magnanimous();
    }

    // Now pass it off to WordPress.
    wp_enqueue_script(
      $name,
      $url,
      $deps,
      $version,
      $in_footer
    );
  }

  // #Thanks: Andrija Naglic
  //
  // This returns a consistent "version" for a file based on its
  // modification time.  For example, a file last modified on 
  // 2016-05-22 12:39:44 will have the version:  
  //
  //    160522.123944
  //
  // If you then work on the file and save it a few minutes later, the new
  // version will be, say, 
  //
  //    160522.124351
  //
  // Once the plugin is installed, it will have a fixed version until a new
  // one overwrites it, so site visitors get a cached version.
  //
  // This is a good default, but feel free to use something like Semantic
  // Versioning for project development, of course.
  private function _default_file_version ($fn) {
    return date('ymd.Gis', filemtime($fn));
  }

  private function _full_or_minified_path ($basename) {
    // By default, prefer the basename with a .js suffix.
    $fn = "$basename.js";

    // If there's a minified version of the script, use that instead.
    if (file_exists($this->M->path->disk_base() . "resources/js/$basename.min.js")) {
      $fn = "$basename.min.js";
    }

    return $this->M->path->disk_base() . "resources/js/$fn";
  }

  private function _full_or_minified_url ($basename) {
    // By default, prefer the basename with a .js suffix.
    $fn = "$basename.js";
    // If there's a minified version of the script, use that instead.
    if (file_exists($this->M->path->disk_base() . "resources/js/$basename.min.js")) {
      $fn = "$basename.min.js";
    }

    return $this->M->path->url_base() . "resources/" . "js/$fn";
  }

  // ******************************************************
  //
  //  JSON 
  //
  function to_json ($data=null) {
    return json_encode($data);
  }

  function from_json ($json='null') {
    return json_decode($json);
  }

  // ******************************************************
  //
  //  Script Injection
  //

  // This allows to to inject some stuff directly into the page safely.  It
  // assumes jQuery (which is a WordPress default) and only does stuff when
  // the document is ready -- which means after any required scripts have
  // loaded.  Good for widgetry.
  //
  // Usage Example:
  //
  //    Say you have a file, my-widget.js with the following content:
  //
  //      ;// I'm an awesome widget that does junk
  //      var widget = new Widget($params);
  //      widget.run();
  //
  //    It doesn't need to be registered or enqueued with WordPress (though,
  //    in this example, it depends on the Widget.js file, which you should
  //    register).
  //
  //    Let's assume you already registered Widget.js previously in your
  //    javascripts.php file.
  //
  //    Now, in your PHP plugin file, you can do this:
  //
  //      $this->M->js->requires('Widget');
  //      echo $this->M->js->inject('my-widget');
  //
  //    That's it.  You get a one-off script installed.  Note that ->inject
  //    returns a string, but doesn't actually do the printing to the page.
  //    That's a good thing. =)
  //
  //    Alternately, you can include some data to pass to the widget (as a
  //    $params variable in javascript):
  //
  //      echo $this->M->js->inject('my-widget', ['data'=>['a','b','c']];
  //
  //    In this version, the widget can 'magically' use $params.data[1] and
  //    get 'b'.
  //
  //    Finally, you can also request that a wrapper be inserted into the
  //    page (just above the script snippet) with a unique ID that's
  //    accessible to the widget in the form of $params.target_id:
  //
  //      // The 'false' here just means I'm not passing in $params data
  //      // explicitly.
  //      echo $this->M->js->inject('my-widget', false, ['insert_target'=>true]);
  //
  //    Now, the output will look more or less like this:
  //
  //      <div id='sfe_vhwsw08934s'></div>
  //      <script type='text/javascript'>
  //        // ... your script here
  //      </script>
  //
  //    And if your script wants to inject stuff into that div, you can use
  //    the $params.target_id to find it.  For example:
  //
  //      // ... somewhere in your widget
  //      $("#"+$params.target_id).append(...);
  //
  // ### Included Magic
  //
  // $ (jQuery)
  //
  //    Every injected script has jQuery available through the $ variable.
  //    Don't worry, this isn't global--it's lexically scoped within the script
  //    only, so it won't collide with other things.
  //
  // $params
  //
  //    Now, the script also provides some magic in the form of the $data
  //    variable.  No matter what, the script has a $params variable
  //    available to it (don't worry, it's lexically scoped to the one-off
  //    script only).
  //
  //    The $params variable has some built-in stuff you can use in the
  //    script without worry:
  //
  //    $params.target_id
  //      A garaunteed unique id that can be used as a jQuery selector.  For
  //      example, the target_id may be af_sfnm94s.  You can use that in a
  //      div somewhere, then in your script you can do things like
  //
  //        $("#"+$params.target_id).append(...);
  //
  //    $params.instance_id
  //      A different garaunteed unique id.  Use this as the id of a wrapper
  //      element for your widget, if you need one.
  //
  //    Anything else you add to $data will be preserved, and will overwrite
  //    the default $params values that are generated. 
  //
  function inject ($name, $data=false, $options=[]) {
    if (array_key_exists('path', $options)) {
      $script = file_get_contents($options['path']);
    } else {
      $script = file_get_contents($this->_full_or_minified_path($name));
    }

    // If this isn't a request for Magnanimous and also if Magnanimous isn't
    // loaded, then load it.
    if ($name != 'Magnanimous' && !wp_script_is('Magnanimous', 'enqueued')) {
      $this->load_Magnanimous();
    } 

    return $this->inject_str($script, $data, $options);
  }

  // Inect a React app into your WordPress
  function inject_react_app ($name, $data=false, $options=[]) {
    $_name = "react-app/$name/build/bundle";

    if (!array_key_exists('require', $options)) {
      $options['require'] = [];
    }

    
    // *************
    // The expected CSS file is the /build/styles.css file of the react app.
    //
    // This is an ugly hack and I need to fix it to use the ..._or_min
    // function.
    $_styles_file = 
      $this->M->path->disk_base() .
      "resources/js/react-app/$name/build/styles.min.css";

    if (file_exists($_styles_file)) {
      $this->M->css->register(["../js/react-app/$name/build/styles.min" => []]);
      $this->M->css->requires("../js/react-app/$name/build/styles.min");
    }
    else {
      $_styles_file = 
        $this->M->path->disk_base() .
        "resources/js/react-app/$name/build/styles.css";

      if (file_exists($_styles_file)) {
        $this->M->css->register(["../js/react-app/$name/build/styles" => []]);
        $this->M->css->requires("../js/react-app/$name/build/styles");
      }
    }
    // *************

    // Make sure we require the react file.
    array_push(
      $options['require'],
      "react.development",
       "react-dom.development"
     );

    $options['insert_target'] = true;

    return $this->inject($_name, $data, $options);
  }

  function inject_str ($script, $data=false, $options=[]) {
    // We don't want to do this in one step in case we need to grab the
    // target_id from the injection_params to render an inserted target.
    $injection_params = $this->_gen_injection_params($data);
    $json = $this->to_json($injection_params);

    // If a target div was requested, add it.
    $target_div = '';
    if (array_key_exists('insert_target', $options) 
      && $options['insert_target']) {
      $tag = 'div';
      if (array_key_exists('target_tag', $options)) {
        $tag = $options['target_tag'];
      } elseif (gettype($options['insert_target']) == 'string') {
        $tag = $options['insert_target'];
      }

      $target_div = "<$tag id='" . $injection_params['target_id'] . "'></$tag>";
    }

    // If there were any requirements, load 'em up.
    if (array_key_exists('require', $options)) {
      foreach ($options['require'] as $req) {
        $this->M->js->requires($req);
      }
    }

    // The injected script is wrapped in a scoping function that takes three
    // arguments:
    //
    //  jQuery
    //    This just imports jQuery.
    //
    //  $json
    //    Any data that was passed in is here serialized and passed through.
    //
    //  --
    //    This function gently attempts to pass in a client-side Magnanimous
    //    JavaScript object.  If it fails, the result is undefined.  If it
    //    succeeds, it's a Magnanimous JS plugin corresponding to this
    //    plugin.
    $plugin_name = $this->M->path->name;


    return "
    $target_div
    <script type='text/javascript'>" .
      // The outer  wrapper here of document.addEventListener(...) is to
      // ensure that this is only loaded after the page loads.  That's so
      // jQuery is definitely available.
			"document.addEventListener('DOMContentLoaded', function(event) { 
				;(function ($, \$params) {
					 var \$targetSelector = '#' + \$params.target_id;
					 var \$target         = $(\$targetSelector);
					 var reactAppTarget   = document.querySelector(\$targetSelector); 
					 var \$template       = \$params.templates || {};
					 " .
					 // This is extremely important.  It gives us a pseudo-global state
					 // for each app that is available to all included libs.
					 "
					 var \$state = \$params.state || {};
					 " .
					 // This lets us sandbox the code so we don't bleed variables (like
					 // name and template_str in the for loop here).
					 "
					 (function () {
						 for (var name in \$template) { " .
							 // This is necessary to break enclosure and realize each
							 // template string individually.
							 " \$template[name] = (function (template_str) {
								 return function (raw) {
									 return raw ? template_str : $(template_str);
								 };
							 })(\$template[name]);
						 }
					 })();
					 $(document).ready(function () {
						 $script
					 ;});
				})(jQuery, $json);
			});		
    </script>
    ";
  }

	function FormatBacktrace () {
		$result = '';

		foreach (debug_backtrace() as $trace)
		{
			if ($trace['function'] ==__FUNCTION__)
				continue;

			$parameters = is_array($trace['args']) ? implode(", ",$trace['args']) : "";

			if (array_key_exists('class', $trace))
				$result .= sprintf("// %s:%s %s::%s(%s)\n",
				$trace['file'],   
				$trace['line'],    
				$trace['class'],  
				$trace['function'],  
				$parameters);
			else
				$result .= sprintf("// %s:%s %s(%s)\n", 
				$trace['file'], 
				$trace['line'], 
				$trace['function'], 
				$parameters);
		}

		return $result;
	}

  private function _gen_injection_params ($data=false) {
    // create a new $params array that has any default magic like target_id
    // and instance_id.  These will be overwritten if specified from outside
    // in the $data param.
    $params = [
      'target_id'   => $this->M->util->unique_id(),
      'instance_id' => $this->M->util->unique_id(),
    ];

    // If we didn't get data, use an array.
    if (!$data) {
      $data = array();
    }

    // Loop through the $data arg and overwrite anything in $params with the
    // values provided by $data.
    foreach ($data as $key => $value) {
      $params[$key] = $value;
    }

    return $params;
  }

}

?>
