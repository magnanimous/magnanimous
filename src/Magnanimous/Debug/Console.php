<?php
namespace Magnanimous\Debug;

class Console {
	private $M;
	private $template;
	function __construct ($M, $params=[]) {
		$this->M = $M;
		$this->template = <<<EOJS
<script type='text/javascript'>
	console.%s.apply(console, %s);
</script>
EOJS;
	}

	private function emit ($type, ...$args) {
		$json = $this->M->js->to_json($args);
		echo $this->M->util->string->prepare(
			$this->template,
			$type,
			$this->M->js->to_json($args)
		);
	}

	function debug (...$args) { $this->emit ('debug' , ...$args); }
	function log   (...$args) { $this->emit ('log'   , ...$args); }
	function info  (...$args) { $this->emit ('info'  , ...$args); }
	function warn  (...$args) { $this->emit ('warn'  , ...$args); }
	function error (...$args) { $this->emit ('error' , ...$args); }
}

?>
