<?php
namespace Magnanimous\Module;

/***

  // Start with the Magnanimous\Module class.  Create a manifest from there
  // like this:
  $manifest = $module->create_manifest('/.../my-module/manifest.json');
  $manifest->read_file();

  // Once the manifest file is read, you can request resources by type and
  // name.
 
  $manifest->get_resource('css', 'my-pickles');

  // If the manifest has something in it like this:
  //
  //    {
  //      resources: {
  //        "css": {
  //          "paths": "./pub/css",
  //          "suffixes": ["css", "min.css"]
  //        }
  //      }
  //    }
  //
  // should return the disk path something like this (assuming there's a
  // minified version of the css file):
  //
  //    /.../modules/my-module/pub/css/my-pickels.min.css
  //
  // The default configuration is the following.  You don't need to specify
  // a resource type's config unless it differs from this.
  //
  //    {
  //      resources: {
  //        "$type": {
  //          "paths": "./pub/$type",
  //          "suffixes": ["$type", "min.$type"]
  //        }
  //      }
  //    }
  //
 ***/


class Manifest {

  private $M;      // This Magnanimous instance.
  private $config; // Read from the manifest.json file.

  // Relative module path, such as "modules/my-module/"
  private $relative_module_path;
  // Absolute module path, such as "/.../modules/my-module/"
  private $absolute_module_path;
  // Absolute module url, such as "http://.../modules/my-module/"
  private $absolute_module_url;

  // This is private to make it effectively read-only through the
  // get_entrypoint() function.
  private $entrypoint        = null;
  private $distribution_path = "/";

  function __construct ($M, $relative_module_path) {
    $this->M = $M;

    $this->relative_module_path = $relative_module_path;
    $this->absolute_module_path = $M->path->disk_base() . $relative_module_path;
    $this->absolute_module_url  = $M->path->url_base()  . $relative_module_path;
  }

  function getModuleBaseUrl () {
    return $this->absolute_module_url;
  }

  function getModuleDistributionUrl () {
    return $this->absolute_module_url . $this->distribution_path;
  }

  function read_file () {
    $manifest = $this->absolute_module_path . 'manifest.json';
    if (!file_exists($manifest)) {
      $basename = basename($this->absolute_module_path); 
      // TODO  This should all be restructured so that when there's no
      // manifest file, it does The Right Thing with an implicit manifest with
      // sensible defaults.
      echo "<strong>Warning</strong>:  A manifest file is missing for the module '$basename'.";
      return;
    }

    $json         = file_get_contents($manifest);
    $this->config = json_decode($json, true);

    if (!array_key_exists('lib', $this->config)) {
      $this->config['lib'] = [];
    }

    // If we have an entrypoint specified, overwrite the default value.
    if (array_key_exists("entrypoint", $this->config)) {
      $this->entrypoint = $this->config['entrypoint'];
    }

    if (array_key_exists("distribution_path", $this->config)) {
      $this->distribution_path = $this->config['distribution_path'];
      if (!$this->entrypoint) {
        $this->entrypoint = $this->distribution_path . '/index.html';
      }
    }
  }

  private function check_file_exists ($relative_filepath) {
    return file_exists($this->absolute_module_path . $relative_filepath);
  }

  // Given a type and basename, return the full disk path to the resource.
  // For example:
  //
  //    $manifest->resource_path("css", "my-styles");
  //
  // should return something like:
  //
  //    /.../my-module/pub/css/my-styles.min.css
  //
  // Returns null if no valid version of the file is found.
	function resource_path ($type, $basename) {
    $list = $this->generate_file_list($type, $basename);

    foreach ($list as $relative_filepath) {
      if ($this->check_file_exists($relative_filepath)) {
        return $this->absolute_module_path . $relative_filepath;
      }
    }

    return false;
  }

  // Given a type and basename, return the full disk path to the resource.
  // For example:
  //
  //    $manifest->resource_url("css", "my-styles");
  //
  // should return something like:
  //
  //    http://.../my-module/pub/css/my-styles.min.css
  //
  // In some kind of perfect world, Magnanimous would set up some end point
  // that can request resource files.  For example, this resource might be
  // somewhere like:
  //
  //    http://blah.com/resource/fj2948jfw438s
  //
  // And then Magnanimous would process the request and serve it.  This
  // would more or less hide server details from prying eyes.
  //
  // Returns null if no valid version of the file is found.
  function resource_url ($type, $basename) {
    $list = $this->generate_file_list($type, $basename);

    foreach ($list as $relative_filepath) {
      if ($this->check_file_exists($relative_filepath)) {
        return $this->absolute_module_url . $relative_filepath;
      }
    }

    return null;
  }

  // This lets you do things like check if you are using a react app.  That
  // would look like this:  $module->manifest->hasLib('react');
  function hasLib ($name) {
    return (bool) (array_key_exists($name, $this->config['lib']));
  }

  // Generate a config specific to this type.  For example, if the type is
  // 'css', create a config like:
  //
  //  [
  //    'type'     => 'css',
  //    'paths'    => [...],
  //    'suffixes' => [...]
  //  ]
  //
  private function get_resource_type_config ($type) {
    $resources = $this->config['resources'];

    $config    = [
      'type'     => $type,
      'paths'    => [],
      'suffixes' => [],
    ];

    // If we ask for a config for a "foo" type, just populate it with an
    // empty one if there isn't one already.
    if (!array_key_exists($type, $resources)) {
      $resources[$type] = [];
    }

    // We want to ensure that we always have a 'paths' array and a
    // 'suffixes' array.  We do that using this logical process:
    //
    // 1. If there's no 'paths' array, create one.
    // 2. If there is a 'path' value, prepend it to the 'paths' array.
    // 3. If there are no values in the 'suffixes' array, make sure it has
    //    at east the two values: ["min.$type","$type"].  Note that the
    //    first dot is not here; it's the suffix, not the conjoiner.
    //    For 'paths', the default we want is just ["pub/$type/"]

    // Create the suffixes array.
    if (array_key_exists('path', $resources[$type])) {
      array_unshift($config['paths'], $resources[$type]['path']);
    }

    if (array_key_exists('paths', $resources[$type])) {
      if (is_array($resources[$type]['paths'])) {
        $config['paths'] = array_merge(
          $config['paths'],
          $resources[$type]['paths']
        );
      } else {
        array_unshift($config['paths'], $resources[$type]['paths']);
      }
    }

    if (sizeof($config['paths']) == 0) {
      $config['paths'] = [$type];
    }

    // Create the suffixes array.
    if (array_key_exists('suffix', $resources[$type])) {
      array_unshift($config['suffixes'], $resources[$type]['suffix']);
    }

    if (array_key_exists('suffixes', $resources[$type])) {
      if (is_array($resources[$type]['suffixes'])) {
        $config['suffixes'] = array_merge(
          $config['suffixes'],
          $resources[$type]['suffixes']
        );
      } else {
        array_unshift($config['suffixes'], $resources[$type]['suffixes']);
      }
    }

    if (sizeof($config['suffixes']) == 0) {
      $config['suffixes'] = ["min.$type", $type];
    }

    return $config;
  }

  // Priority is paths over suffixes.  That means that if we have, for
  // example, the paths ['a/', 'b/'] and the suffixes ['y', 'z'] for a file
  // called 'my-file', our list will look like this:
  //
  //  [
  //    'a/my-file.y',
  //    'a/my-file.z',
  //    'b/my-file.y',
  //    'b/my-file.z',
  //  ]
  //
  // Notice that we permute the file suffixes *within* the paths, not the
  // other way around.
  //
  private function generate_file_list ($type, $basename) {
    $list = [];

    // Get a config that definitely has a 'paths' array and a 'suffixes'
    // array.
    $config   = $this->get_resource_type_config($type);
    $paths    = $config['paths'];
    $suffixes = $config['suffixes'];

    foreach ($paths as $path) {
      foreach ($suffixes as $suffix) {
        array_push($list, "$path/$basename.$suffix");
      }
    }

    return $list;
  }

  function get_entrypoint () {
    return $this->entrypoint;
  }

}

?>
