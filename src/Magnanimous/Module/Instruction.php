<?php
namespace Magnanimous\Module;

/*****
 * Some dreamcode:
 *
 * $instructions = Magnanimous\Module\Instruction->from_string($txt);
 *
 * $instruction = $instructions[0];
 * // Render the instruction however it should be included in the parent
 * // document.
 * $instruction->render();
 *
 * // Show various properties of the instruction.
 * $instruction->directive
 * $instruction->attribute('type'); // returns 'css', for example.
 *
 ****/

class Instruction {

  private $M;
  private $module;

  public $source;
  public $directive;
  public $attributes;
  public $render_string;
  public $data;

  function __construct ($M, $module) {
    $this->M             = $M;
    $this->module        = $module;
    $this->directive     = null;
    $this->attributes    = [];
    $this->data          = [];
    $this->render_string = "";
  }

  static function from_string ($txt, $module, $M, $params=[]) {
    $data = null;
    if (array_key_exists('data', $params)) {
      $data = $params['data'];
    }

    $instructions = [];

    $valid_instructions = "(include|link)";
		$pattern            = "#<\?($valid_instructions)\s.*\?>#si";
    preg_match_all("#(<\?$valid_instructions.*?>)#si", $txt, $matches);

    foreach ($matches[0] as $match) {
      $instruction       = new self($M, $module);
      $instruction->data = $data;
      
      $instructions[$match] = $instruction->interp_instruction($match);
    }

    return $instructions;
  }

  function attribute ($name, $value=null) {
    if (!$value) {
      return $this->attributes[$name];
    }

    $this->attributes[$name] = $value;

    return $this;
  }

  // Given an instruction string like this:
  //   "<?link type=\"css\" src='main' ? >"
  //
  // return 
  private function interp_instruction ($instruction) {
    $this->source = $instruction;

    // Returns the directive or nothing.
    $this->directive = $this->extract_directive($instruction);

    if (!$this->directive) {
      return $this;
    }

    // This is now an associative array of attributes.
    $this->attributes = $this->extract_attributes($instruction);

    // Now we render it into a string.  This is a little bit dicey to do
    // here... really big files will be stored in memory.  So, let's not do
    // it with really big files?
    $this->render_string = $this->render_instruction();

    return $this;
  }

  private function render_instruction () {

    if (!$this->attribute('type') || !$this->attribute('src')) {
      return '';
    }

    switch ($this->directive) {
      case 'link':
        return $this->render_link();

      case 'include':
        return $this->render_include();

      default: 
        return $this->render_error();
        break;
    }
  }

  // A link looks like this:
  //
  //    <?link type="css" src="main" ? >
  //
  // and should render like this:
  //
  //    <link rel="stylesheet" type="text/css" href="theme.css">
  //
  private function render_link () {

    $str = '<link rel="stylesheet" type="text/css" href="';

    if ($this->attribute('type') == 'css') {
      $str .= $this->module->manifest->resource_url(
        'css',
        $this->attribute('src')
      );
    }
    else if ($this->attribute('type') == 'js') {
      $str .= $this->module->manifest->resource_url(
        'js',
        $this->attribute('src')
      );
    }

    $str .= '"></link>';
    return $str;
  }

  // An include looks like this:
  //
  //    <?include type="css" src="main" ? >
  //
  // and should render like this (for a css example):
  //
  //    <style type="text/css">
  //      html, body { margin: 0; }
  //      div#content { width: 920px; }
  //    </style>
  //
  private function render_include () {
    $filepath = $this->module->manifest->resource_path(
      $this->attribute('type'),
      $this->attribute('src')
    );

    if (!$filepath) {
      return "Cannot find a valid " . $this->attribute('type')
        . ' file named ' . $this->attribute('src');
    }

    // At this moment we're not doing any kind of recursive render... just
    // plug it in.
    $contents = file_get_contents($filepath);

    if ($this->attribute('type') == 'css') {
      $contents = "<style type='text/css'>$contents</style>";
    } 
    else if ($this->attribute('type') == 'js') {
      // We want to include jQuery before each injection.  Doing it this way
      // has a few advantages:
      //  1. Multiple inclusions this way only have a single query since the
      //     browser caches the url after the first one.
      //  2. It ensures jQuery is available for the Magnanimous wrapper.
      $contents = '<script type="text/javascript" '
        . 'src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js">'
        . '</script>'
        . $this->M->js->inject_str($contents, ['data' => $this->data]);
    } else {
      $contents = "Magnanimous Error:  Unrecognized module include type.";
    }


    return $contents;
  }

  private function render_error () {
    return "Unable to render instruction.";
  }

  private function extract_directive ($instruction) {
    $valid_instructions = "(include|link)";
		$pattern = "#<\?($valid_instructions)\s.*\?>#si";

		preg_match($pattern, $instruction, $matches);

    if ($matches && $matches[1]) {
      return $matches[1];
    } else {
      return;
    }
  }

  // php> $txt = '<?include foo="bar" baz=\'bof\'? >';
  // php> $pattern = '#(\w*)=((?:\'|").*?(?:\'|"))#si';
  // php> preg_match_all($pattern, $txt, $matches);
  // php> print_r($matches);
  // Array (
  //     [0] => Array
  //         (
  //             [0] => foo="bar"
  //             [1] => baz='bof'
  //         )
  // 
  //     [1] => Array
  //         (
  //             [0] => foo
  //             [1] => baz
  //         )
  // 
  //     [2] => Array
  //         (
  //             [0] => "bar"
  //             [1] => 'bof'
  //         )
  // )
  private function extract_attributes ($instruction) {
		$pattern = '#(\w*)=((?:\'|").*?(?:\'|"))#si';
    preg_match_all($pattern, $instruction, $matches);

    if ($matches && $matches[0]) {
      $results = [];
      foreach ($matches[0] as $match) {
        $parts = explode('=', $match, 2);
        $parts[1] = preg_replace('/^["\']/si', "", $parts[1]);
        $parts[1] = preg_replace('/["\']$/si', "", $parts[1]);
        $results[$parts[0]] = $parts[1];
      }
      return $results;
    } else {
      return [];
    }
  }

  /****
   *
   *  $foo = <<<EOP
   *  <?include
   *    type="css"
   *    src="bar"
   *  ?> 
   *    This is the <em>thing</em> here.
   *    
   *  <?include
   *    type="js"
   *    src="baz"
   *  ?> 
   *  EOP;
   *  
   *  preg_match_all("#(<\?include.*?>)#si", $foo, $out);
   *  echo json_encode($out,JSON_PRETTY_PRINT);
   *
   */
}

?>
