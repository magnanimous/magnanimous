<?php
namespace Magnanimous\Module\HTML;

// Here's a complete list of HTML4 and HTML5 tags that have URL-like
// attributes.
//
//   <a href=url>
//   <applet codebase=url>
//   <area href=url>
//   <base href=url>
//   <blockquote cite=url>
//   <body background=url>
//   <del cite=url>
//   <form action=url>
//   <frame longdesc=url>
//      and <frame src=url>
//   <head profile=url>
//   <iframe longdesc=url>
//      and <iframe src=url>
//   <img longdesc=url> 
//      and <img src=url>
//      and <img usemap=url>
//   <input src=url>
//      and <input usemap=url>
//   <ins cite=url>
//   <link href=url>
//   <object classid=url> 
//      and <object codebase=url>
//      and <object data=url>
//      and <object usemap=url>
//   <q cite=url>
//   <script src=url>
// 
// HTML 5 adds a few (and HTML5 seems to not use some of the ones above as well):
//   <audio src=url>
//   <button formaction=url>
//   <command icon=url>
//   <embed src=url>
//   <html manifest=url>
//   <input formaction=url>
//   <source src=url>
//   <track src=url>
//   <video poster=url>
//      and <video src=url>
//  
// These aren't clean URLs:
//
//   <img srcset="url1 resolution1 url2 resolution2">
//   <source srcset="url1 resolution1 url2 resolution2">
//   <object archive=url> or <object archive="url1 url2 url3">
//   <applet archive=url> or <applet archive=url1,url2,url3>
//   <meta http-equiv="refresh" content="seconds; url">
// 
// SVGs can also contain links to resources:
//   <svg><image href="url" /></svg>
// 
// In addition, the style attribute can contain css declarations with one or
// several urls. For example: <div style="background: url(image.png)">
//



// ****************
// A generalized function to handle replacements will take a script tag,
// an attribute name, and a url prefix from which the new relative url
// should be directed.  Of course, it also needs the DOM object.
function rebaseHtmlUrls ($html, $prefix, $options=[]) {
	$dom = new \domDocument;
	$dom->loadHTML($html);
	$dom->preserveWhiteSpace = false;

	// Now we can simply add as many of these mutators as we like and the
	// document will be mutated.
	foreach ([
		"a"          => "href",
		"applet"     => "codebase",
		"area"       => "href",
		"base"       => "href",
		"blockquote" => "cite",
		"body"       => "background",
		"del"        => "cite",
		"form"       => "action",
		"frame"      => "longdesc",
		"frame"      => "src",
		"head"       => "profile",
		"iframe"     => "longdesc",
		"iframe"     => "src",
		"img"        => "longdesc",
		"img"        => "src",
		"img"        => "usemap",
		"input"      => "src",
		"input"      => "usemap",
		"ins"        => "cite",
		"link"       => "href",
		"object"     => "classid",
		"object"     => "codebase",
		"object"     => "data",
		"object"     => "usemap",
		"q"          => "cite",
		"script"     => "src",
		"audio"      => "src",
		"button"     => "formaction",
		"embed"      => "src",
		"html"       => "manifest",
		"input"      => "formaction",
		"source"     => "src",
		"track"      => "src",
		"video"      => "poster",
		"video"      => "src",
	] as $tag => $attribute) {
		$dom = rebaseTagUrls($dom, $tag, $attribute, $prefix, $options);
	}

	// Re-render this DOM as a string and return it.
	$dom->formatOutput = true;
	return 
		getInnerHtml($dom->getElementsByTagName('head')[0]) .
		getInnerHtml($dom->getElementsByTagName('body')[0]);
}

// Given a domDocument element, get the innerHTML rendering.
function getInnerHtml ($element) {
	if (!$element) {
    return "";
	}

	$innerHTML = ""; 
	$children  = $element->childNodes;

	foreach ($children as $child) { 
		$innerHTML .= $element->ownerDocument->saveHTML($child);
	}

	return $innerHTML;
}

function rebaseTagUrls ($dom, $tag_name, $attribute, $prefix, $options=[]) {
	$tags = $dom->getElementsByTagName($tag_name);

	foreach ($tags as $tag) {
		$url = $tag->getAttribute($attribute);
    if (!$url) {
      continue;
    }
		$tag->setAttribute($attribute, rebaseUrl($url, $prefix, $options));
	}

	// This function actually modifies $dom in place because it's a reference
	// to an object, but I don't like that very much, so we're returning
	// explicitly here as well.
	return $dom;
}

function isRelativeUrl ($url) {
	$pattern1 = "/^[\.a-zA-Z0-9]/";
	$pattern2 = "/:\/\//";
	return (bool) (
		$url && preg_match($pattern1, $url) && !preg_match($pattern2, $url)
	);
}

function isAbsoluteUrl ($url) {
  // starts with a protocol, like:  http://...
  $pattern1 = "/^(\w+):\/\//";
  // starts with a slash, like: /foo/bar/baz.html
	$pattern2 = "/^\//";

	return (bool) (
		$url && (preg_match($pattern1, $url) || preg_match($pattern2, $url))
	);
}

function rebaseUrl ($url, $prefix, $options=[]) {

	if (array_key_exists('relative', $options) && isRelativeUrl($url)) {
    // If the prefix has terminal slashes, remove them.
		$prefix = preg_replace('/\/*$/'  , ''         , $prefix);
    // If the URL starts with ./ then get rid of it.
		$url = preg_replace('/^\.\//' , ''         , $url);
    // If the URL starts with a slash, nuke it.
		$url = preg_replace('/^/'     , "$prefix/" , $url);

    // If we don't have a ./ or a http then add a ./ to make it relative
		if (!preg_match('/\.\//', $url) && !preg_match('/^http/', $prefix)) {
			$url = "./$url";
		}

    // Return the URL.
    return $url;
	}

	if (array_key_exists('absolute', $options) && isAbsoluteUrl($url)) {
    // If the prefix has terminal slashes, remove them.
		$prefix = preg_replace('/\/*$/', '', $prefix);
    // Remove any initial slashes.
		$url = preg_replace('/^\/*/', '', $url);
    // Remove the initial protocol stuff.
		$url = preg_replace('/^http:\/\//', '', $url);
    // Add the prefix.
		$url = preg_replace('/^/', "$prefix/" , $url);

		if (!preg_match('/\.\//', $url) && !preg_match('/^http/', $prefix)) {
			$url = "./$url";
		}

    return $url;
	}

	return $url;
}

